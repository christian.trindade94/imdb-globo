describe("authenticate", () => {
	const dashboardPage = "http://localhost:3000/dashboard";

	it("sshould redirect unauthenticated users to the login page", () => {
		cy.visit(dashboardPage);

		cy.url().should("include", "/login");
	});
});
