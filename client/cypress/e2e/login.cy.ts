describe("Login Page", () => {
	const loginUrl = "http://localhost:3000/login";
	const homeUrl = "http://localhost:3000/";

	it("should display an error message with invalid credentials", () => {
		cy.visit(loginUrl);

		cy.get('input[placeholder="Email"]').type("invalid-username");
		cy.get('input[placeholder="Password"]').type("invalid-password");
		cy.get('button[type="submit"]').click();
		cy.contains("Email or password incorrect").should("be.visible");
	});

	it("should successfully log in with valid credentials", () => {
		cy.visit(loginUrl);

		cy.get('input[placeholder="Email"]').type("admin@imdbglobo.com");
		cy.get('input[placeholder="Password"]').type("password123");

		cy.get('button[type="submit"]').click();

		cy.contains("Signout").should("be.visible");
	});
});
