describe("votes", () => {
	it("should vote for a movie and return a error message", () => {
		cy.request({
			method: "POST",
			url: "http://localhost:8000/api/votes",
			body: {
				movie_id: 1,
				score: 4,
			},
			failOnStatusCode: false,
		}).then((response) => {
			expect(response.status).to.eq(401);
			expect(response.body).to.have.property("message");
		});
	});

	it("should login and try to vote for a movie and retunr status 200", () => {
		cy.visit("http://localhost:3000/login");
		cy.get('input[placeholder="Email"]').type("admin@imdbglobo.com");
		cy.get('input[placeholder="Password"]').type("password123");
		cy.get('button[type="submit"]').click();

		cy.contains("Signout").should("be.visible");

		cy.request({
			method: "POST",
			url: "http://localhost:8000/api/votes",
			body: {
				movie_id: 1,
				score: 4,
			},
			failOnStatusCode: false,
		}).then((response) => {
			expect(response.status).to.eq(200);
		});
	});
});
