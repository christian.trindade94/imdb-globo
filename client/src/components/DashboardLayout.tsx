import React from "react";

import { Container, DashboardColumn, MainColumn, MenuItem } from "./ui";

interface DashboardProps {
	itens: {
		label: string;
		action: () => void;
	}[];
	children: React.ReactNode;
}

const DashboardLayout: React.FC<DashboardProps> = ({ itens, children }) => {
	const renderItens = itens.map((item) => <MenuItem onClick={item.action}>{item.label}</MenuItem>);

	return (
		<Container>
			<DashboardColumn>{renderItens}</DashboardColumn>
			<MainColumn>{children}</MainColumn>
		</Container>
	);
};

export default DashboardLayout;
