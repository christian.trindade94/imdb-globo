import * as React from "react";

import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

interface LoadingProps {
	open: boolean;
	setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const Loading: React.FC<LoadingProps> = ({ open, setOpen }) => {
	const handleClose = () => {
		setOpen(false);
	};

	return (
		<Backdrop
			sx={{ color: "#ffffff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
			open={open}
			onClick={handleClose}
		>
			<CircularProgress color="inherit" />
		</Backdrop>
	);
};

export default Loading;
