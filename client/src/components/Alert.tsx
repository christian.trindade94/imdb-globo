import React from "react";
import { Button, Dialog, DialogTitle, DialogContent, DialogActions } from "@material-ui/core";

interface AlertProps {
	open: boolean;
	title?: string;
	message?: string;
	buttons?: IAlertButtons[];
	onClose: () => void;
}

const Alert: React.FC<AlertProps> = ({ open, title, message, buttons, onClose }) => {
	const renderButtons = () =>
		buttons?.map((button) => <Button onClick={button.action}>{button.title}</Button>);

	return (
		<Dialog open={open} onClose={onClose}>
			<DialogTitle>{title}</DialogTitle>
			<DialogContent>{message}</DialogContent>
			{renderButtons()}
		</Dialog>
	);
};

export default Alert;
