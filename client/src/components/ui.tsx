import styled from "styled-components";

export const ToolbarWrapper = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 7px 16px 7px 16px;
`;

export const Button = styled.button`
	padding: 8px 16px;
	border: none;
	background-color: transparent;
	color: inherit;
	font-size: 16px;
	font-weight: bold;
	cursor: pointer;
`;

export const HomeButton = styled(Button)`
	margin-right: auto;
`;

export const LoginButton = styled(Button)``;

export const StarRatingContainer = styled.div`
	display: inline-flex;

	.star {
		color: #c0c0c0;
		cursor: pointer;
		transition: color 200ms ease-in-out;

		&:hover,
		&.filled {
			color: #ffdf00;
		}
	}
`;

export const Container = styled.div`
	display: flex;
	background-color: #eeeaea;
	width: 95%;
	margin: auto;
	border-radius: 5px;
	min-height: 90vh;
`;

export const DashboardColumn = styled.div`
	width: 200px;
	padding: 16px;
	background-color: #dbd9d9;
`;

export const MainColumn = styled.div`
	flex-grow: 1;
	padding: 16px;
`;

export const MenuItem = styled.div`
	cursor: pointer;
	padding: 8px;
	color: #686666;
	font-size: large;
	font-weight: 600;

	&:hover {
		background-color: #bbb6b6;
	}
`;

export const MoviePhoto = styled.img`
	width: 100%;
	max-width: 400px;
	height: auto;
`;
