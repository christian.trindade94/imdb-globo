import * as React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { vote } from "../services/api";

import Alert from "./Alert";
import Star from "./Star";

import { StarRatingContainer } from "../components/ui";

interface StarRatingProps {
	movieId: number;
	value: number;
	afterVote?: () => any;
}

const StarRating: React.FC<StarRatingProps> = ({ value, movieId, afterVote }) => {
	const navigate = useNavigate();
	const [hovered, setHovered] = useState<number | null>(null);

	const [openAlert, setOpenAlert] = useState(false);
	const [alertData, setAlertData] = useState<IAlertData>({});

	const handleStarEnter = (index: number) => {
		setHovered(index);
	};

	const handleStarLeave = () => {
		setHovered(null);
	};

	const handleClick = (index: number) => {
		vote({
			movie_id: movieId,
			score: index + 1,
		})
			.then((response) => {
				setAlertData({
					title: "Thanks",
					message: "your vote was counted",
					buttons: [
						{
							title: "Ok",
							action: () => setOpenAlert(false),
						},
					],
				});

				setOpenAlert(true);

				afterVote?.();
			})
			.catch((error) => {
				const status = error.response.status;

				if (status == 401) {
					setAlertData({
						title: "Error",
						message: "You must be logged in to vote",
						buttons: [
							{
								title: "SigIn",
								action: () => navigate("/login"),
							},
							{
								title: "No thanks",
								action: () => setOpenAlert(false),
							},
						],
					});

					setOpenAlert(true);
				}
			});
	};

	const renderStars = () =>
		[...Array(4)].map((_, index) => {
			const filled = hovered !== null ? index <= hovered : index < value;
			return (
				<Star
					key={index}
					filled={filled}
					onMouseEnter={() => handleStarEnter(index)}
					onMouseLeave={handleStarLeave}
					onClick={() => handleClick(index)}
				/>
			);
		});

	return (
		<>
			<StarRatingContainer>{renderStars()}</StarRatingContainer>
			<Alert
				open={openAlert}
				title={alertData.title}
				message={alertData.message}
				buttons={alertData.buttons}
				onClose={() => setOpenAlert(false)}
			/>
		</>
	);
};

export default StarRating;
