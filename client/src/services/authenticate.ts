import { fetchRole } from "./api";

import { UserRole } from "../types/enums";

const USER_DATA_KEY = process.env.REACT_APP_SER_DATA_KEY as string;

export const getLocalUserData = () => JSON.parse(window.localStorage.getItem(USER_DATA_KEY)!);
export const isAuthenticated = () => !!getLocalUserData();
export const removeLocalUserData = () => window.localStorage.removeItem(USER_DATA_KEY);

export const verifyAdminCredentials = async () => {
	const credentials = await fetchRole()
		.then((response) => response.data)
		.catch((error) => false);

	const isAdmin = credentials.role == UserRole.ADMIN;
	return isAdmin;
};
