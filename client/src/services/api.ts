import axios from "axios";

const USER_DATA_KEY = process.env.REACT_APP_SER_DATA_KEY as string;

const api = axios.create({
	baseURL: process.env.REACT_APP_API,
	withCredentials: true,
});

axios.interceptors.response.use(
	(response) => response,
	async (error) => {
		const status = error.response.status;

		if (status === 401 || status === 402) {
			await signout();
			window.location.href = "/login";
		}
	}
);

const fetchMovies = (data?: fetchMoviesProps) => {
	let rote = `/movies`;

	if (data?.id) {
		rote += `/${data?.id}`;
	}

	return api.get(rote, { params: data }).then((response) => response);
};

const deleteMovie = (id: number) => {
	return api.delete(`/movies/${id}`).then((response) => response);
};

const createMovie = (data: ICreateMovie) => {
	return api.post("/movies", data).then((response) => response);
};

const editMovie = (data: ICreateMovie, id: number) => {
	return api.patch(`/movies/${id}`, data).then((response) => response);
};

const fetchGenres = () => {
	return api.get("/genres").then((response) => response);
};

const fetchActors = () => {
	return api.get("/actors").then((response) => response);
};

const fetchDirectors = () => {
	return api.get("/directors").then((response) => response);
};

//the TOKEN is received via COOKIE HTTP ONLY and manipulation by the user is not possible
const signin = (data: ICredential) => {
	return api.post("/users/authenticate", data).then((response) => {
		window.localStorage.setItem(USER_DATA_KEY, "true");
	});
};

const signout = () => {
	return api.get("/users/deauthenticate").then((response) => response);
};

const fetchRole = () => {
	return api.get("/users/role").then((response) => response);
};

const fetchUsers = () => {
	return api.get("/users").then((response) => response);
};

const createUser = (data: ICreateUser) => {
	return api.post("/users", data).then((response) => response);
};

const editUser = (data: ICreateUser, userId: number) => {
	return api.patch(`/users/${userId}`, data).then((response) => response);
};

const deactivateUser = (id: number) => {
	return api.delete(`/users/${id}`).then((response) => response);
};

const activatedUser = (id: number) => {
	return api.post(`/users/activate/${id}`).then((response) => response);
};

const fetchGenders = () => {
	return api.get("/genders").then((response) => response);
};

const fetchRoles = () => {
	return api.get("/roles").then((response) => response);
};

const vote = (data: IVote) => {
	return api.post("/votes", data).then((response) => response);
};

const upload = (formData: FormData) => {
	return api.post("/upload", formData).then((response) => response);
};

export {
	api,
	fetchMovies,
	fetchGenres,
	fetchActors,
	fetchDirectors,
	signin,
	signout,
	fetchRole,
	vote,
	fetchUsers,
	deactivateUser,
	activatedUser,
	fetchGenders,
	fetchRoles,
	createUser,
	editUser,
	deleteMovie,
	createMovie,
	editMovie,
	upload,
};
