import * as React from "react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import { verifyAdminCredentials } from "../../services/authenticate";

import GlobalStyle from "../../theme";

import Alert from "../../components/Alert";
import Toolbar from "../../components/ToolBar";
import DashboardLayout from "../../components/DashboardLayout";

import UsersTable from "./components/users";
import MoviesTable from "./components/movies";

const User: React.FC = () => {
	const navigate = useNavigate();

	const [roleVerified, setRoleVerified] = useState<boolean>(false);
	const [openAlert, setOpenAlert] = useState<boolean>(false);
	const [alertData, setAlertData] = useState<IAlertData>({});

	const [activeItem, setActiveItem] = useState<string>("Users");

	const itens = [
		{ label: "Users", action: () => setActiveItem("Users") },
		{ label: "Movies", action: () => setActiveItem("Movies") },
	];

	useEffect(() => {
		checkAdmin();
	}, []);

	const checkAdmin = async () => {
		const isAdmin = await verifyAdminCredentials();

		if (!isAdmin) {
			navigate("/login");
		} else {
			setRoleVerified(true);
		}
	};

	const renderContentByActiveItem = () => {
		switch (activeItem) {
			case "Users":
				return <UsersTable setOpenAlert={setOpenAlert} setAlertData={setAlertData} />;
			default:
				return <MoviesTable setOpenAlert={setOpenAlert} setAlertData={setAlertData} />;
		}
	};

	return roleVerified ? (
		<>
			<GlobalStyle />
			<Toolbar />
			<DashboardLayout itens={itens}>{renderContentByActiveItem()}</DashboardLayout>

			<Alert
				open={openAlert}
				title={alertData.title}
				message={alertData.message}
				buttons={alertData.buttons}
				onClose={() => setOpenAlert(false)}
			/>
		</>
	) : (
		<></>
	);
};

export default User;
