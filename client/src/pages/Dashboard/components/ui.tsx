import styled from "styled-components";

export const FormWrapper = styled.form`
	display: flex;
	flex-direction: column;
	max-width: 400px;
	margin: 0 auto;
	color: black;
	width: 100%;
`;

export const Label = styled.label`
	margin-bottom: 8px;
`;

export const Input = styled.input`
	padding: 8px;
	margin-bottom: 16px;
`;

export const Select = styled.select`
	padding: 8px;
	margin-bottom: 16px;
`;

export const Button = styled.button`
	padding: 8px 16px;
	background-color: #007bff;
	color: #fff;
	border: none;
	cursor: pointer;
`;

export const MoviePhoto = styled.img`
	cursor: pointer;
	width: 100%;
	max-width: 400px;
	height: auto;
`;

export const MovieContainer = styled.div`
	margin-left: auto;
	margin-right: auto;
	display: flex;
	padding: 16px;
`;
