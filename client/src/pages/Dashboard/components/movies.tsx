import * as React from "react";
import { useState, useEffect } from "react";
import {
	TableContainer,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
	IconButton,
	Button,
} from "@material-ui/core";
import { Edit, Delete } from "@material-ui/icons";

import {
	fetchMovies,
	deleteMovie,
	fetchActors,
	fetchDirectors,
	fetchGenres,
} from "../../../services/api";

import MovieForm from "./movie";

interface MovieProps {
	setOpenAlert: React.Dispatch<React.SetStateAction<boolean>>;
	setAlertData: React.Dispatch<React.SetStateAction<IAlertData>>;
}

const Movie: React.FC<MovieProps> = ({ setOpenAlert, setAlertData }) => {
	const [movies, setMovies] = useState<Movie[]>([]);
	const [genres, setGenres] = useState<Genre[]>([]);
	const [actors, setActors] = useState<Actor[]>([]);
	const [directors, setDirectors] = useState<Director[]>([]);
	const [selectedMovie, setSelectedMovie] = useState<Movie>();
	const [create, setCreate] = useState<boolean>(false);

	useEffect(() => {
		getMovies();
		getGenres();
		getActors();
		getDirectors();
	}, []);

	const getMovies = async () => {
		fetchMovies().then((response) => setMovies(response.data));
	};

	const getGenres = async () => {
		fetchGenres().then((response) => setGenres(response.data));
	};

	const getActors = async () => {
		fetchActors().then((response) => setActors(response.data));
	};

	const getDirectors = async () => {
		fetchDirectors().then((response) => setDirectors(response.data));
	};

	const fetchDeleteMovie = async (id: number) => {
		deleteMovie(id).then(() => getMovies());

		setAlertData({
			title: "Deleted",
			message: "movie successfully deleted",
			buttons: [
				{
					title: "Ok",
					action: () => setOpenAlert(false),
				},
			],
		});
	};

	const confirmDeleteMovie = (id: number) => {
		setAlertData({
			title: "delete movie",
			message: "you want to delete this movie?",
			buttons: [
				{
					title: "Delete",
					action: () => fetchDeleteMovie(id),
				},
				{
					title: "Cancel",
					action: () => setOpenAlert(false),
				},
			],
		});

		setOpenAlert(true);
	};

	const renderGenreLabelById = (genreId: number) => {
		const genre = genres.find((genre) => genre.id === genreId);
		return genre ? genre.label : "";
	};

	const goback = () => {
		setSelectedMovie(undefined);
		setCreate(false);
		getMovies();
	};
	const renderCreateOrList = () => {
		if (create) {
			return (
				<>
					<Button onClick={goback}>Return</Button>
					<MovieForm
						setOpenAlert={setOpenAlert}
						setAlertData={setAlertData}
						goBack={goback}
						genres={genres}
						actors={actors}
						directors={directors}
						selectedMovie={selectedMovie}
					/>
				</>
			);
		} else {
			return (
				<>
					<Button onClick={() => setCreate(true)}>Create</Button>
					<TableContainer>
						<Table>
							<TableHead>
								<TableRow>
									<TableCell>Name</TableCell>
									<TableCell>Description</TableCell>
									<TableCell>Genre</TableCell>
									<TableCell>Score</TableCell>
									<TableCell>Edit</TableCell>
									<TableCell>Action</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{movies.map((movie) => (
									<TableRow key={movie.id}>
										<TableCell>{movie.name}</TableCell>
										<TableCell>{movie.description}</TableCell>
										<TableCell>{renderGenreLabelById(movie.genre_id)}</TableCell>
										<TableCell>{movie.average_vote_score}</TableCell>
										<TableCell>
											<IconButton
												onClick={() => {
													setCreate(true);
													setSelectedMovie(movie);
												}}
											>
												<Edit />
											</IconButton>
										</TableCell>
										<TableCell>
											<IconButton onClick={() => confirmDeleteMovie(movie.id)}>
												<Delete />
											</IconButton>
										</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</TableContainer>
				</>
			);
		}
	};

	return <>{renderCreateOrList()}</>;
};

export default Movie;
