import React, { useState } from "react";

import { createUser, editUser } from "../../../services/api";

import { FormWrapper, Label, Input, Select, Button } from "./ui";

interface UserFormProps {
	id?: number;
	roles: Role[];
	genders: Gender[];
	goBack: Function;
	selectedUser?: User;
	setOpenAlert: React.Dispatch<React.SetStateAction<boolean>>;
	setAlertData: React.Dispatch<React.SetStateAction<IAlertData>>;
}

const UserForm: React.FC<UserFormProps> = ({
	roles,
	genders,
	goBack,
	setAlertData,
	setOpenAlert,
	selectedUser,
}) => {
	const initialFormData: ICreateUser = {
		name: selectedUser?.name || "",
		lastname: selectedUser?.lastname || "",
		username: selectedUser?.username || "",
		password: selectedUser?.password || "",
		role_id: selectedUser?.role_id || 1,
		email: selectedUser?.email || "",
		photo: selectedUser?.photo || "photo.jpeg",
		gender_id: selectedUser?.gender_id || 1,
		disabled: false,
	};

	const [formData, setFormData] = useState<ICreateUser>(initialFormData);

	const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
		const { name, value } = e.target;

		setFormData((prevState) => ({
			...prevState,
			[name]: value,
		}));
	};

	const checkErrors = (error: any) => {
		const status = error.response.status;
		const data = error.response.data;

		if (status === 400) {
			let errorMessage;
			if (data.message) {
				errorMessage = data.message;
			} else {
				const constraints = Object.keys(data[0].constraints);
				errorMessage = data[0].constraints[constraints[0]];
			}

			setAlertData({
				title: "invalid fields",
				message: errorMessage,
				buttons: [
					{
						title: "Ok",
						action: () => setOpenAlert(false),
					},
				],
			});

			setOpenAlert(true);
		}
	};

	const fetchCreateUser = () => {
		formData.role_id = Number(formData.role_id);
		formData.gender_id = Number(formData.gender_id);

		createUser(formData)
			.then((response) => {
				setAlertData({
					title: "user created",
					message: "the was created",
					buttons: [
						{
							title: "Ok",
							action: () => setOpenAlert(false),
						},
					],
				});

				setOpenAlert(true);
				goBack();
			})
			.catch((error) => checkErrors(error));
	};

	const fetchEditUser = () => {
		formData.role_id = Number(formData.role_id);
		formData.gender_id = Number(formData.gender_id);

		editUser(formData, selectedUser?.id!)
			.then((response) => {
				setAlertData({
					title: "user edited",
					message: "the was edited",
					buttons: [
						{
							title: "Ok",
							action: () => setOpenAlert(false),
						},
					],
				});

				setOpenAlert(true);
				goBack();
			})
			.catch((error) => checkErrors(error));
	};

	const handleSubmit = (e: React.FormEvent) => {
		e.preventDefault();
		if (selectedUser?.id) {
			fetchEditUser();
		} else {
			fetchCreateUser();
		}
	};

	return (
		<FormWrapper onSubmit={handleSubmit}>
			<Label>Name:</Label>
			<Input required type="text" name="name" value={formData.name} onChange={handleChange} />
			<Label>Lastname:</Label>
			<Input
				required
				type="text"
				name="lastname"
				value={formData.lastname}
				onChange={handleChange}
			/>

			<Label>Username:</Label>
			<Input
				required
				type="text"
				name="username"
				value={formData.username}
				onChange={handleChange}
			/>

			<Label>Password:</Label>
			<Input type="password" name="password" value={formData.password} onChange={handleChange} />
			<Label>Email:</Label>

			<Input required type="email" name="email" value={formData.email} onChange={handleChange} />
			<Label>Gender:</Label>
			<Select required name="gender_id" onChange={handleChange} value={formData.gender_id}>
				{genders.map((genders) => (
					<option key={genders.id} value={genders.id}>
						{genders.label}
					</option>
				))}
			</Select>
			<Label>Role:</Label>
			<Select required name="role_id" onChange={handleChange} value={formData.role_id}>
				{roles.map((role) => (
					<option key={role.id} value={role.id}>
						{role.label}
					</option>
				))}
			</Select>

			<Button type="submit">Submit</Button>
		</FormWrapper>
	);
};

export default UserForm;
