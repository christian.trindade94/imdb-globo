import * as React from "react";
import { useState, useRef } from "react";
import _ from "lodash";

import { createMovie, editMovie, upload } from "../../../services/api";

import Loading from "../../../components/Loading";

import { FormWrapper, Label, Input, Select, Button, MoviePhoto, MovieContainer } from "./ui";

interface MovieFormProps {
	id?: number;
	genres: Genre[];
	actors: Actor[];
	directors: Director[];
	goBack: Function;
	selectedMovie?: Movie;
	setOpenAlert: React.Dispatch<React.SetStateAction<boolean>>;
	setAlertData: React.Dispatch<React.SetStateAction<IAlertData>>;
}

const MovieForm: React.FC<MovieFormProps> = ({
	genres,
	actors,
	directors,
	goBack,
	setAlertData,
	setOpenAlert,
	selectedMovie,
}) => {
	const initialFormData: ICreateMovie = {
		name: selectedMovie?.name || "",
		description: selectedMovie?.description || "",
		photo: selectedMovie?.photo || "photo.jpeg",
		genre_id: selectedMovie?.genre_id || 1,
		directors: [],
		actors: [],
	};

	const [loading, setLoading] = useState(false);

	const [formData, setFormData] = useState<ICreateMovie>(initialFormData);

	const inputFileRef = useRef<HTMLInputElement>(null);

	const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
		const { name, value } = e.target;

		if (name === "actors" || name === "directors") {
			const integerValue = Number(value);
			const personSelected = formData[name].findIndex((person) => person.id === integerValue);
			const newPersons = _.cloneDeep(formData[name]);

			if (personSelected !== -1) {
				newPersons.splice(personSelected, 1);
			} else {
				newPersons.push({
					id: integerValue,
				});
			}

			setFormData((prevState) => ({
				...prevState,
				[name]: newPersons,
			}));
		} else {
			setFormData((prevState) => ({
				...prevState,
				[name]: value,
			}));
		}
	};

	const checkErrors = (error: any) => {
		const status = error.response.status;
		const data = error.response.data;

		if (status === 400) {
			let errorMessage;
			if (data.message) {
				errorMessage = data.message;
			} else {
				const constraints = Object.keys(data[0].constraints);
				errorMessage = data[0].constraints[constraints[0]];
			}

			setAlertData({
				title: "invalid fields",
				message: errorMessage,
				buttons: [
					{
						title: "Ok",
						action: () => setOpenAlert(false),
					},
				],
			});

			setOpenAlert(true);
		}
	};

	const fetchCreateMovie = () => {
		formData.genre_id = Number(formData.genre_id);
		formData.average_vote_score = 0;

		createMovie(formData)
			.then((response) => {
				setAlertData({
					title: "Movie created",
					message: "the movie was created",
					buttons: [
						{
							title: "Ok",
							action: () => setOpenAlert(false),
						},
					],
				});

				setOpenAlert(true);
				goBack();
			})
			.catch((error) => checkErrors(error));
	};

	const fetchEditMovie = () => {
		formData.genre_id = Number(formData.genre_id);

		editMovie(formData, selectedMovie?.id!)
			.then((response) => {
				setAlertData({
					title: "movie edited",
					message: "the movie was edited",
					buttons: [
						{
							title: "Ok",
							action: () => setOpenAlert(false),
						},
					],
				});

				setOpenAlert(true);
				goBack();
			})
			.catch((error) => checkErrors(error));
	};

	const handleSubmit = (e: React.FormEvent) => {
		e.preventDefault();
		if (selectedMovie?.id) {
			fetchEditMovie();
		} else {
			fetchCreateMovie();
		}
	};

	const extractPersonId = (person: { id: number }[]) => {
		return person.map((actor) => String(actor.id));
	};

	const handleOpenFilePicker = () => {
		if (inputFileRef.current) {
			inputFileRef.current.click();
		}
	};

	const handleFileSelect = (event: React.ChangeEvent<HTMLInputElement>) => {
		const file = event.target.files?.[0];

		if (file) {
			uploadPhoto(file);
		}
	};

	const uploadPhoto = async (file: File) => {
		const formData = new FormData();
		formData.append("file", file);

		setLoading(true);
		await upload(formData).then((response) => {
			setFormData((prevState) => ({
				...prevState,
				photo: response.data.url,
			}));

			inputFileRef.current!.value = "";
			return response;
		});

		setLoading(false);
	};

	return (
		<MovieContainer>
			<MoviePhoto src={formData.photo} alt="Movie Photo" onClick={handleOpenFilePicker} />
			<input
				type="file"
				ref={inputFileRef}
				onChange={handleFileSelect}
				accept=".png, .jpeg, .jpg"
				style={{ display: "none" }}
			/>
			<FormWrapper onSubmit={handleSubmit}>
				<Label>Name:</Label>
				<Input required type="text" name="name" value={formData.name} onChange={handleChange} />
				<Label>Description:</Label>
				<Input
					required
					type="text"
					name="description"
					value={formData.description}
					onChange={handleChange}
				/>

				<Label>Genre:</Label>
				<Select required name="genre_id" onChange={handleChange} value={formData.genre_id}>
					{genres.map((genre) => (
						<option key={genre.id} value={genre.id}>
							{genre.label}
						</option>
					))}
				</Select>

				<Select
					multiple
					name="actors"
					onChange={handleChange}
					value={extractPersonId(formData.actors)}
				>
					{actors.map((actor) => (
						<option key={actor.id} value={actor.id}>
							{actor.name}
						</option>
					))}
				</Select>
				<Select
					multiple
					name="directors"
					onChange={handleChange}
					value={extractPersonId(formData.directors)}
				>
					{directors.map((director) => (
						<option key={director.id} value={director.id}>
							{director.name}
						</option>
					))}
				</Select>

				<Button type="submit">Submit</Button>
			</FormWrapper>
			<Loading open={loading} setOpen={setLoading} />
		</MovieContainer>
	);
};

export default MovieForm;
