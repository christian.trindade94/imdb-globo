import * as React from "react";
import { useState, useEffect } from "react";
import {
	TableContainer,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
	IconButton,
	Button,
} from "@material-ui/core";
import { Edit, Delete, Create } from "@material-ui/icons";

import {
	fetchUsers,
	fetchGenders,
	fetchRoles,
	deactivateUser,
	activatedUser,
} from "../../../services/api";

import UserForm from "./user";

interface UserProps {
	setOpenAlert: React.Dispatch<React.SetStateAction<boolean>>;
	setAlertData: React.Dispatch<React.SetStateAction<IAlertData>>;
}

const User: React.FC<UserProps> = ({ setOpenAlert, setAlertData }) => {
	const [users, setUsers] = useState<User[]>([]);
	const [genders, setGenders] = useState<Gender[]>([]);
	const [roles, setRoles] = useState<Role[]>([]);
	const [selectedUser, setSelectedUser] = useState<User>();
	const [create, setCreate] = useState<boolean>(false);

	useEffect(() => {
		getUsers();
		getGenders();
		getRoles();
	}, []);

	const getUsers = async () => {
		fetchUsers().then((response) => setUsers(response.data));
	};

	const getGenders = async () => {
		fetchGenders().then((response) => setGenders(response.data));
	};

	const getRoles = async () => {
		fetchRoles().then((response) => setRoles(response.data));
	};

	const deleteUser = async (id: number) => {
		deactivateUser(id).then(() => getUsers());

		setAlertData({
			title: "Deactivated",
			message: "user successfully deactivated",
			buttons: [
				{
					title: "Ok",
					action: () => setOpenAlert(false),
				},
			],
		});
	};

	const activateUser = async (id: number) => {
		activatedUser(id).then(() => getUsers());

		setAlertData({
			title: "Activated",
			message: "user successfully activated",
			buttons: [
				{
					title: "Ok",
					action: () => setOpenAlert(false),
				},
			],
		});

		getUsers();
	};

	const confirmDeleteUser = (id: number) => {
		setAlertData({
			title: "Deactivate user",
			message: "you want to deactivate this user?",
			buttons: [
				{
					title: "Delete",
					action: () => deleteUser(id),
				},
				{
					title: "Cancel",
					action: () => setOpenAlert(false),
				},
			],
		});

		setOpenAlert(true);
	};

	const confirmActivateUser = (id: number) => {
		setAlertData({
			title: "Activate user",
			message: "you want to activate this user?",
			buttons: [
				{
					title: "Activate",
					action: () => activateUser(id),
				},
				{
					title: "Cancel",
					action: () => setOpenAlert(false),
				},
			],
		});

		setOpenAlert(true);
	};

	const renderActiveDeactiveButton = (disabled: boolean, userId: number) => {
		if (disabled) {
			return (
				<TableCell>
					<IconButton onClick={() => confirmActivateUser(userId)}>
						<Create />
					</IconButton>
				</TableCell>
			);
		} else {
			return (
				<TableCell>
					<IconButton onClick={() => confirmDeleteUser(userId)}>
						<Delete />
					</IconButton>
				</TableCell>
			);
		}
	};

	const goback = () => {
		setSelectedUser(undefined);
		setCreate(false);
		getUsers();
	};

	const renderCreateOrList = () => {
		if (create) {
			return (
				<>
					<Button onClick={goback}>Return</Button>
					<UserForm
						setOpenAlert={setOpenAlert}
						setAlertData={setAlertData}
						goBack={goback}
						genders={genders}
						roles={roles}
						selectedUser={selectedUser}
					/>
				</>
			);
		} else {
			return (
				<>
					<Button onClick={() => setCreate(true)}>Create</Button>
					<TableContainer>
						<Table>
							<TableHead>
								<TableRow>
									<TableCell>Name</TableCell>
									<TableCell>Lastname</TableCell>
									<TableCell>Username</TableCell>
									<TableCell>Email</TableCell>
									<TableCell>Role</TableCell>
									<TableCell>Disabled</TableCell>
									<TableCell>Edit</TableCell>
									<TableCell>Action</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{users.map((user) => (
									<TableRow key={user.id}>
										<TableCell>{user.name}</TableCell>
										<TableCell>{user.lastname}</TableCell>
										<TableCell>{user.username}</TableCell>
										<TableCell>{user.email}</TableCell>
										<TableCell>{user.role.label}</TableCell>
										<TableCell>{String(user.disabled)}</TableCell>
										<TableCell>
											<IconButton
												onClick={() => {
													setCreate(true);
													setSelectedUser(user);
												}}
											>
												<Edit />
											</IconButton>
										</TableCell>
										{renderActiveDeactiveButton(user.disabled, user.id)}
									</TableRow>
								))}
							</TableBody>
						</Table>
					</TableContainer>
				</>
			);
		}
	};

	return <>{renderCreateOrList()}</>;
};

export default User;
