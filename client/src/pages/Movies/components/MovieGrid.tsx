import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";

import NoResultImg from "../../../assets/no_result.svg";

import StarRating from "../../../components/StarRating";

import { GridWrapper, MovieCard, NoResultContainer } from "./ui";

interface Movie {
	id: number;
	name: string;
	average_vote_score: number;
	photo: string;
}

interface MovieGridProps {
	movies: Movie[];
	onLoadMore: () => void;
	hasMore: boolean;
}

const MovieGrid: React.FC<MovieGridProps> = ({ movies, onLoadMore, hasMore }) => {
	const navigate = useNavigate();

	const renderMovies = () =>
		movies.map((movie) => (
			<MovieCard key={movie.id} onClick={() => navigate(`/movie/${movie.id}`)}>
				<img src={movie.photo} alt={movie.name} />
				<div className="details">
					<h3>{movie.name}</h3>
					<StarRating movieId={movie.id} value={movie.average_vote_score} />
				</div>
			</MovieCard>
		));

	const renderNoResult = () => (
		<NoResultContainer>
			<h2>No results found</h2>
			<img src={NoResultImg} />
		</NoResultContainer>
	);

	const render = movies.length ? renderMovies : renderNoResult;

	return (
		<InfiniteScroll
			dataLength={movies.length}
			next={onLoadMore}
			hasMore={hasMore}
			loader={<></>}
			style={{ overflow: "visible", maxHeight: "100vh" }}
		>
			<GridWrapper>{render()}</GridWrapper>
		</InfiniteScroll>
	);
};

export default MovieGrid;
