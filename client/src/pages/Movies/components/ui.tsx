import styled from "styled-components";

export const GridWrapper = styled.div`
	display: grid;
	grid-template-columns: repeat(auto-fill, minmax(220px, 1fr));
	gap: 1em;
	padding: 2em;
`;

export const MovieCard = styled.div`
	cursor: pointer;
	background-color: #202020;
	border-radius: 8px;
	overflow: hidden;
	box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.5);
	transition: transform 0.3s ease-in-out, box-shadow 0.3s ease-in-out;

	img {
		display: block;
		width: 100%;
		transition: opacity 0.3s ease-in-out;
	}

	h3 {
		margin: 0;
		color: #fff;
		transition: color 0.3s ease-in-out;
	}

	.details {
		padding: 1em;
	}

	&:hover {
		transform: scale(1.05);
		box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.6);

		h3 {
			color: #f0f0f0;
		}

		img {
			opacity: 0.9;
		}
	}
`;

export const SearchInput = styled.input`
	padding: 8px 16px;
	border: none;
	border-radius: 10px;
	font-size: 16px;
	width: 30%;
	margin-top: 5px;
`;

export const Select = styled.select`
	padding: 8px 16px;
	border: none;
	border-radius: 10px;
	font-size: 16px;
	margin-top: 4px;
	background-color: white;
	margin-right: 8px;
	width: 10%;
`;

export const Button = styled.button`
	padding: 8px 16px;
	border: none;
	background-color: transparent;
	color: inherit;
	font-size: 16px;
	font-weight: bold;
	cursor: pointer;
`;

export const NoResultContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 90vw;
	justify-content: center;
	img {
		max-width: 30%;
	}
`;
