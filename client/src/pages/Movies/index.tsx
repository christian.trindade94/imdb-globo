import * as React from "react";
import { useState, useEffect } from "react";
import _, { get, set } from "lodash";

import { fetchMovies, fetchGenres, fetchActors, fetchDirectors } from "../../services/api";

import GlobalStyle from "../../theme";

import Toolbar from "../../components/ToolBar";
import Loading from "../../components/Loading";

import MovieGrid from "./components/MovieGrid";
import { SearchInput, Select, Button } from "./components/ui";

const Movie: React.FC = () => {
	const limit = 10;

	const [loading, setLoading] = useState<boolean>(false);

	const [movies, setMovies] = useState<Movie[]>([]);

	const [page, setPage] = useState<number>(1);
	const [hasMore, setHasMore] = useState<boolean>(true);

	const [genres, setGenres] = useState<Genre[]>([]);
	const [actors, setActors] = useState<Actor[]>([]);
	const [directors, setDirectors] = useState<Director[]>([]);

	const [search, setSearch] = useState<string>("");
	const [selectedGenre, setSelectedGenre] = useState<string>("");
	const [selectedActor, setSelectedActor] = useState<string>("");
	const [selectedDirector, setSelectedDirector] = useState<string>("");

	const debouncedSearchMovies = React.useRef(
		_.debounce(async (search) => {
			setSearch(search);

			if (!search) {
				setMovies([]);
				setPage(1);
				setHasMore(true);

				getMovies({ page: 1, limit });
			}
		}, 500)
	).current;

	useEffect(() => {
		getMovies({ page, limit });
		getGenres();
		getActors();
		getDirectors();
	}, []);

	useEffect(() => {
		searchMoviesByFilters();
	}, [search, selectedActor, selectedDirector, selectedGenre]);

	const searchMoviesByFilters = () => {
		const filtered = search || selectedActor || selectedDirector || selectedGenre;

		if (filtered) {
			setMovies([]);
			setPage(1);
			setHasMore(true);

			const searchData = {
				page: 1,
				limit,
				name: search,
				genre: selectedGenre,
				actors: selectedActor,
				directors: selectedDirector,
			};

			getMovies(searchData);
		}
	};

	const checkEndList = (responseLength: number) => {
		if (responseLength < limit) {
			setHasMore(false);
		}
	};

	const getGenres = async () => {
		await fetchGenres().then((response) => {
			const newGenres = response.data as Genre[];
			setGenres(newGenres);
		});
	};

	const getActors = async () => {
		await fetchActors().then((response) => {
			const newActors = response.data as Actor[];
			setActors(newActors);
		});
	};

	const getDirectors = async () => {
		await fetchDirectors().then((response) => {
			const newDirectors = response.data as Director[];
			setDirectors(newDirectors);
		});
	};

	const getMovies = async (data: fetchMoviesProps) => {
		setLoading(true);
		await fetchMovies(data).then((response) => {
			const newMovies = response.data as Movie[];
			setMovies((prevState) => [...prevState, ...newMovies]);
			checkEndList(response.data.length);
		});
		setLoading(false);
	};

	const handleLoadMore = () => {
		setPage(page + 1);

		const searchData = {
			page: page + 1,
			limit,
		};

		getMovies(searchData);
	};

	const clearFilters = () => {
		setSearch("");
		setSelectedGenre("");
		setSelectedActor("");
		setSelectedDirector("");
		setMovies([]);
		setPage(1);
		setHasMore(true);

		getMovies({ page: 1, limit });
	};

	return (
		<>
			<GlobalStyle />
			<Toolbar>
				<Button onClick={clearFilters}>Clear</Button>
				<Select value={selectedGenre} onChange={(e) => setSelectedGenre(e.target.value)}>
					<option value="" disabled selected>
						Genre
					</option>

					{genres.map((genre) => (
						<option key={genre.id} value={genre.id}>
							{genre.label}
						</option>
					))}
				</Select>
				<Select value={selectedActor} onChange={(e) => setSelectedActor(e.target.value)}>
					<option value="" disabled selected>
						Actor
					</option>
					{actors.map((actor) => (
						<option key={actor.id} value={actor.id}>
							{actor.name} {actor.lastname}
						</option>
					))}
				</Select>
				<Select value={selectedDirector} onChange={(e) => setSelectedDirector(e.target.value)}>
					<option value="" disabled selected>
						Director
					</option>
					{directors.map((director) => (
						<option key={director.id} value={director.id}>
							{director.name} {director.lastname}
						</option>
					))}
				</Select>

				<SearchInput
					onChange={(e) => debouncedSearchMovies(e.target.value)}
					placeholder="Search"
				/>
			</Toolbar>

			<MovieGrid movies={movies} onLoadMore={handleLoadMore} hasMore={hasMore} />

			<Loading open={loading} setOpen={setLoading} />
		</>
	);
};

export default Movie;
