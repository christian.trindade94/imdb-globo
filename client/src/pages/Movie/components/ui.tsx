import styled from "styled-components";

export const MovieContainer = styled.div`
	height: 100%;
	margin-left: auto;
	margin-right: auto;
	display: flex;
	padding: 16px;
`;

export const MovieDetailsContainer = styled.div`
	display: flex;
	flex-direction: column;
	flex: 1;
	padding-left: 16px;
	justify-content: space-between;
`;

export const MovieTitle = styled.h2`
	margin-bottom: 16px;
`;

export const MovieDescription = styled.p`
	margin-bottom: 16px;
`;

export const PersonsContainer = styled.div`
	margin-bottom: 16px;
`;

export const Person = styled.div`
	display: flex;
	align-items: center;
	margin-bottom: 8px;
`;

export const PersonPhoto = styled.img`
	max-width: 48px;
	margin-right: 8px;
`;

export const PersonName = styled.span`
	font-weight: 300;
`;

export const MoviePhoto = styled.img`
	width: 100%;
	max-width: 400px;
	height: auto;
`;
