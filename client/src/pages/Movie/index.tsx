import React from "react";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import { fetchMovies } from "../../services/api";

import GlobalStyle from "../../theme";

import Toolbar from "../../components/ToolBar";
import Loading from "../../components/Loading";
import StarRating from "../../components/StarRating";

import {
	MovieContainer,
	MovieDetailsContainer,
	MovieTitle,
	MovieDescription,
	PersonsContainer,
	Person,
	PersonPhoto,
	PersonName,
	MoviePhoto,
} from "./components/ui";

const Movie: React.FC = () => {
	const { id } = useParams();

	const [loading, setLoading] = useState<boolean>(false);

	const [movieData, setMovieData] = useState<Movie>();

	useEffect(() => {
		getMovie();
	}, []);

	const getMovie = async () => {
		const numberId = parseInt(id as string);
		setLoading(true);
		await fetchMovies({ id: numberId }).then((response) => setMovieData(response.data));
		setLoading(false);
	};

	const renderPerson = (data: Director[] | Actor[]) =>
		data.map((actor) => (
			<Person key={actor.id}>
				<PersonPhoto src={actor.photo} alt={`${actor.name} ${actor.lastname}`} />
				<PersonName>{`${actor.name} ${actor.lastname}`}</PersonName>
			</Person>
		));

	return (
		<>
			<Toolbar />
			<GlobalStyle />
			{movieData && (
				<MovieContainer>
					<MoviePhoto src={movieData.photo} alt={movieData.name} />
					<MovieDetailsContainer>
						<div>
							<h4>{movieData.gender.label}</h4>
							<MovieTitle>{movieData.name}</MovieTitle>
							<MovieDescription>{movieData.description}</MovieDescription>
							<StarRating
								afterVote={getMovie}
								movieId={movieData.id}
								value={movieData.average_vote_score}
							/>
						</div>

						<div>
							<PersonsContainer>
								<h4>Actors:</h4>
								{renderPerson(movieData.actors)}

								<h4>Directors:</h4>
								{renderPerson(movieData.directors)}
							</PersonsContainer>
						</div>
					</MovieDetailsContainer>
				</MovieContainer>
			)}

			<Loading open={loading} setOpen={setLoading} />
		</>
	);
};

export default Movie;
