import styled from "styled-components";

export const PageWrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	height: 80vh;
	overflow-y: hidden;
`;

export const LoginForm = styled.form`
	display: flex;
	flex-direction: column;
	padding: 16px;
	background-color: #f5f5f5;
	border-radius: 8px;
`;

export const Input = styled.input`
	padding: 8px;
	margin-bottom: 16px;
	border: 1px solid #ccc;
	border-radius: 4px;
`;

export const Button = styled.button`
	padding: 8px;
	border: none;
	border-radius: 4px;
	background-color: #f5c518;
	font-weight: bolder;
	cursor: pointer;
`;
