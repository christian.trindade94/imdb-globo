import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import { signin } from "../../services/api";

import GlobalStyle from "../../theme";

import Alert from "../../components/Alert";
import Toolbar from "../../components/ToolBar";

import { PageWrapper, LoginForm, Input, Button } from "./components/ui";

const Login: React.FC = () => {
	const navigate = useNavigate();

	const [openAlert, setOpenAlert] = useState(false);
	const [alertData, setAlertData] = useState<IAlertData>({});

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setEmail(event.target.value);
	};

	const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setPassword(event.target.value);
	};

	const handleSubmit = (event: React.FormEvent) => {
		event.preventDefault();

		const credentials = {
			email,
			password,
		};

		signin(credentials)
			.then((response) => {
				navigate("/");
			})
			.catch(() => {
				setAlertData({
					title: "Error",
					message: "Email or password incorrects",
					buttons: [
						{
							title: "Ok",
							action: () => setOpenAlert(false),
						},
					],
				});

				setOpenAlert(true);
			});
	};

	return (
		<>
			<GlobalStyle />
			<Toolbar />
			<PageWrapper>
				<LoginForm onSubmit={handleSubmit}>
					<Input type="text" placeholder="Email" value={email} onChange={handleEmailChange} />
					<Input
						type="password"
						placeholder="Password"
						value={password}
						onChange={handlePasswordChange}
					/>
					<Button type="submit">Login</Button>
				</LoginForm>

				<Alert
					open={openAlert}
					title={alertData.title}
					message={alertData.message}
					buttons={alertData.buttons}
					onClose={() => setOpenAlert(false)}
				/>
			</PageWrapper>
		</>
	);
};

export default Login;
