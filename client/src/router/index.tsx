import React from "react";
import { Fragment, useEffect, useState } from "react";
import {
	BrowserRouter as Router,
	Route,
	Routes,
	Navigate,
	Outlet,
	RouteProps,
} from "react-router-dom";

import Home from "../pages/Movies";
import MovieDetail from "../pages/Movie";
import Login from "../pages/Login";

import Dashboard from "../pages/Dashboard";

const AppRouter: React.FC = () => {
	return (
		<Router>
			<Fragment>
				<Routes>
					<Route path="/" Component={Home} />
					<Route path="/movie/:id" Component={MovieDetail} />
					<Route path="/login" Component={Login} />
					<Route path="/dashboard" element={<Dashboard />} />
				</Routes>
			</Fragment>
		</Router>
	);
};

export default AppRouter;
