/// <reference types="react-scripts" />

type fetchMoviesProps = {
	id?: number;
	name?: string;
	genre?: string;
	actors?: string;
	directors?: string;
	page?: number;
	limit?: number;
};

interface IVote {
	movie_id: number;
	score: number;
}

interface IAlertButtons {
	title: string;
	action: () => void;
}
[];

interface IAlertData {
	title?: string;
	message?: string;
	buttons?: IAlertButtons[];
}

interface ICredential {
	email: string;
	password: string;
}

interface Genre {
	id: number;
	label: string;
	createdDate: string;
	updatedDate: string;
}

interface Actor {
	id: number;
	name: string;
	lastname: string;
	description: string;
	photo: string;
	gender_id: number;
	createdDate: string;
	updatedDate: string;
}

interface Director {
	id: number;
	name: string;
	lastname: string;
	description: string;
	photo: string;
	gender_id: number;
	createdDate: string;
	updatedDate: string;
}

interface Movie {
	id: number;
	name: string;
	description: string;
	photo: string;
	genre_id: number;
	average_vote_score: number;
	createdDate: string;
	updatedDate: string;
	gender: Genre;
	actors: Actor[];
	directors: Director[];
}

interface Gender {
	id: number;
	label: string;
	createdDate: string;
	updatedDate: string;
}

interface Role {
	id: number;
	label: string;
	createdDate: string;
	updatedDate: string;
}

interface User {
	id: number;
	name: string;
	lastname: string;
	username: string;
	password: string;
	role_id: number;
	email: string;
	photo: string;
	gender_id: number;
	disabled: boolean;
	gender: Gender;
	role: Role;
}

interface ICreateUser {
	name: string;
	lastname: string;
	username: string;
	password: string;
	role_id: number;
	email: string;
	photo: string;
	gender_id: number;
	disabled: boolen;
}

interface ICreateMovie {
	name: string;
	description: string;
	photo: string;
	genre_id: number;
	average_vote_score?: number;
	actors: {
		id: number;
	}[];
	directors: {
		id: number;
	}[];
}
