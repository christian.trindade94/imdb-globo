import { MigrationInterface, QueryRunner } from "typeorm";

export class InsertRoles1700851873315 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`INSERT INTO roles (label) VALUES ('user')`);
		await queryRunner.query(`INSERT INTO roles (label) VALUES ('admin')`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`DELETE FROM roles WHERE label IN ('user', 'admin')`);
	}
}
