import { MigrationInterface, QueryRunner } from "typeorm";

export class InsertGenders1700851788675 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Cisgender woman')`);
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Cisgender man')`);
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Transgender woman')`);
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Transgender man')`);
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Non-binary gender')`);
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Agender')`);
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Gender-fluid')`);
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Bigender')`);
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Transsexual woman')`);
		await queryRunner.query(`INSERT INTO genders (label) VALUES ('Transsexual man')`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`DELETE FROM genders WHERE label IN ('Cisgender woman', 'Cisgender man', 'Transgender woman', 'Transgender man', 'Non-binary gender', 'Agender', 'Gender-fluid', 'Bigender', 'Transsexual woman', 'Transsexual man')`
		);
	}
}
