import { MigrationInterface, QueryRunner } from "typeorm";

export class InsertDirectors1700851868345 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('Christopher', 'Nolan', 'Renowned director known for Inception and The Dark Knight Trilogy.', 'https://imdbglobo.s3.amazonaws.com/christopher-nolan.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('Quentin', 'Tarantino', 'Acclaimed filmmaker with a unique style, known for Pulp Fiction and Kill Bill.', 'https://imdbglobo.s3.amazonaws.com/quentin-tarantino.png', 1)`
		);
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('Steven', 'Spielberg', 'Legendary director with a vast filmography including Jurassic Park and Schindlers List.', 'https://imdbglobo.s3.amazonaws.com/steven-spielberg.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('Martin', 'Scorsese', 'Academy Award-winning director known for The Departed and Goodfellas.', 'https://imdbglobo.s3.amazonaws.com/martin-scorsese.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('James', 'Cameron', 'Filmmaker and explorer known for Titanic and Avatar.', 'https://imdbglobo.s3.amazonaws.com/james-cameron.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('Alfred', 'Hitchcock', 'Master of suspense with classics like Psycho and Rear Window.', 'https://imdbglobo.s3.amazonaws.com/alfred-hitchcock.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('Greta', 'Gerwig', 'Talented director and actress known for Lady Bird and Little Women.', 'https://imdbglobo.s3.amazonaws.com/greta-gerwig.jpg', 2)`
		);
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('Jordan', 'Peele', 'Director and writer acclaimed for Get Out and Us.', 'https://imdbglobo.s3.amazonaws.com/jordan-peele.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('Ava', 'DuVernay', 'Director and producer known for Selma and A Wrinkle in Time.', 'https://imdbglobo.s3.amazonaws.com/ava-duvernay.jpg', 2)`
		);
		await queryRunner.query(
			`INSERT INTO directors (name, lastname, description, photo, gender_id) VALUES ('Hayao', 'Miyazaki', 'Renowned Japanese animator and director of Spirited Away and My Neighbor Totoro.', 'https://imdbglobo.s3.amazonaws.com/hayao-miyazaki.jpeg', 1)`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`DELETE FROM directors WHERE name IN ('Christopher', 'Quentin', 'Steven', 'Martin', 'James', 'Alfred', 'Greta', 'Jordan', 'Ava', 'Hayao')`
		);
	}
}
