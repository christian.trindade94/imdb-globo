import { MigrationInterface, QueryRunner } from "typeorm";

export class InsertAdmin1700852137503 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            INSERT INTO users (name, lastname, username, password, role_id, email, photo, gender_id, disabled)
            VALUES ('admin', 'Admin', 'admin', '$2a$10$BYNlcIrwl/Ka.Sq13n85qevHPp2G3CqpR.p9ZlIv6RZkxUvpVmB9i', '2', 'admin@imdbglobo.com', 'photo.jpg', '2', false)
        `);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            DELETE FROM users
            WHERE email = 'admin@imdbglobo.com'
        `);
	}
}
