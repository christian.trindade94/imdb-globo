import { MigrationInterface, QueryRunner } from "typeorm";

export class InsertUser1700928609806 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            INSERT INTO users (name, lastname, username, password, role_id, email, photo, gender_id, disabled)
            VALUES ('user', 'User', 'user', '$2a$10$BYNlcIrwl/Ka.Sq13n85qevHPp2G3CqpR.p9ZlIv6RZkxUvpVmB9i', '1', 'user@imdbglobo.com', 'photo.jpg', '2', false)
        `);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            DELETE FROM users
            WHERE email = 'user@imdbglobo.com'
        `);
	}
}
