import { MigrationInterface, QueryRunner } from "typeorm";

export class InsertGenres1700851851518 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Action')`);
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Drama')`);
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Comedy')`);
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Science Fiction')`);
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Horror')`);
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Romance')`);
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Adventure')`);
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Thriller')`);
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Fantasy')`);
		await queryRunner.query(`INSERT INTO genres (label) VALUES ('Mystery')`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`DELETE FROM genres WHERE label IN ('Action', 'Drama', 'Comedy', 'Science Fiction', 'Horror', 'Romance', 'Adventure', 'Thriller', 'Fantasy', 'Mystery')`
		);
	}
}
