import { MigrationInterface, QueryRunner } from "typeorm";

export class InsertActors1700851857975 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Tom', 'Hanks', 'Award-winning actor known for roles in Forrest Gump and Cast Away.', 'https://imdbglobo.s3.amazonaws.com/tomhanks.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Meryl', 'Streep', 'Acclaimed actress with a diverse range of roles.', 'https://imdbglobo.s3.amazonaws.com/meryl-streep.png', 1)`
		);
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Leonardo', 'DiCaprio', 'Oscar-winning actor known for Titanic and The Revenant.', 'https://imdbglobo.s3.amazonaws.com/leonardo-dicaprio.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Jennifer', 'Lawrence', 'Versatile actress with roles in The Hunger Games and Silver Linings Playbook.', 'https://imdbglobo.s3.amazonaws.com/jennifer-lawrence.jpg', 2)`
		);
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Robert', 'Downey Jr.', 'Iconic actor best known for his portrayal of Iron Man in the Marvel Cinematic Universe.', 'https://imdbglobo.s3.amazonaws.com/robert-downey-jr.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Natalie', 'Portman', 'Talented actress with roles in Black Swan and V for Vendetta.', 'https://imdbglobo.s3.amazonaws.com/natalie-portman.jpg', 2)`
		);
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Johnny', 'Depp', 'Versatile actor known for his roles in Pirates of the Caribbean and Edward Scissorhands.', 'https://imdbglobo.s3.amazonaws.com/johnny-depp.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Angelina', 'Jolie', 'Award-winning actress and humanitarian known for Maleficent and Girl, Interrupted.', 'https://imdbglobo.s3.amazonaws.com/angelina-jolie.jpg', 1)`
		);
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Brad', 'Pitt', 'Versatile actor with roles in Fight Club and Once Upon a Time in Hollywood.', 'https://imdbglobo.s3.amazonaws.com/brad-pitt.jpg', 2)`
		);
		await queryRunner.query(
			`INSERT INTO actors (name, lastname, description, photo, gender_id) VALUES ('Scarlett', 'Johansson', 'Popular actress known for her roles in The Avengers and Lost in Translation.', 'https://imdbglobo.s3.amazonaws.com/scarlett-johansson.jpg', 1)`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`DELETE FROM actors WHERE name IN ('Tom', 'Meryl', 'Leonardo', 'Jennifer', 'Robert', 'Natalie', 'Johnny', 'Angelina', 'Brad', 'Scarlett')`
		);
	}
}
