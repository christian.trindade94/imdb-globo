import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeVoteScoreType1700851883397 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE \`movies\` DROP COLUMN \`average_vote_score\``);
		await queryRunner.query(`ALTER TABLE \`movies\` ADD \`average_vote_score\` float NOT NULL`);
		await queryRunner.query(`ALTER TABLE \`votes\` DROP COLUMN \`score\``);
		await queryRunner.query(`ALTER TABLE \`votes\` ADD \`score\` float NOT NULL`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE \`votes\` DROP COLUMN \`score\``);
		await queryRunner.query(`ALTER TABLE \`votes\` ADD \`score\` int NOT NULL`);
		await queryRunner.query(`ALTER TABLE \`movies\` DROP COLUMN \`average_vote_score\``);
		await queryRunner.query(`ALTER TABLE \`movies\` ADD \`average_vote_score\` int NOT NULL`);
	}
}
