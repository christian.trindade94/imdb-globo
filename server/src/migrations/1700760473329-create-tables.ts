import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTables1700760473329 implements MigrationInterface {
    name = 'CreateTables1700760473329'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`genders\` (\`id\` int NOT NULL AUTO_INCREMENT, \`label\` varchar(255) NOT NULL, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`genres\` (\`id\` int NOT NULL AUTO_INCREMENT, \`label\` varchar(255) NOT NULL, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`roles\` (\`id\` int NOT NULL AUTO_INCREMENT, \`label\` varchar(255) NOT NULL, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`users\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`lastname\` varchar(255) NOT NULL, \`username\` varchar(255) NOT NULL, \`password\` varchar(255) NOT NULL, \`role_id\` int NOT NULL, \`email\` varchar(255) NOT NULL, \`photo\` varchar(255) NOT NULL, \`gender_id\` int NOT NULL, \`disabled\` tinyint NOT NULL DEFAULT 0, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`actors\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`lastname\` varchar(255) NOT NULL, \`description\` text NOT NULL, \`photo\` varchar(255) NOT NULL, \`gender_id\` int NOT NULL, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`movies\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`description\` varchar(255) NOT NULL, \`photo\` varchar(255) NOT NULL, \`genre_id\` int NOT NULL, \`average_vote_score\` int NOT NULL, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`directors\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`lastname\` varchar(255) NOT NULL, \`description\` varchar(255) NOT NULL, \`photo\` varchar(255) NOT NULL, \`gender_id\` int NOT NULL, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`votes\` (\`id\` int NOT NULL AUTO_INCREMENT, \`user_id\` int NOT NULL, \`movie_id\` int NOT NULL, \`score\` int NOT NULL, \`createdDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedDate\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`movies_actors_actors\` (\`moviesId\` int NOT NULL, \`actorsId\` int NOT NULL, INDEX \`IDX_638b1d6f6929495fa5b87206da\` (\`moviesId\`), INDEX \`IDX_6f9bbef3136f7efc40a5a55886\` (\`actorsId\`), PRIMARY KEY (\`moviesId\`, \`actorsId\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`movies_directors_directors\` (\`moviesId\` int NOT NULL, \`directorsId\` int NOT NULL, INDEX \`IDX_668ef403881ed50222bd69d60c\` (\`moviesId\`), INDEX \`IDX_bec316d1f8c152edabafd5ad4e\` (\`directorsId\`), PRIMARY KEY (\`moviesId\`, \`directorsId\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`users\` ADD CONSTRAINT \`FK_a2cecd1a3531c0b041e29ba46e1\` FOREIGN KEY (\`role_id\`) REFERENCES \`genders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`users\` ADD CONSTRAINT \`FK_7fc7be0985c48fa965c80fc8775\` FOREIGN KEY (\`gender_id\`) REFERENCES \`genders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`actors\` ADD CONSTRAINT \`FK_10c7c6cb20ef75e11f1050a8018\` FOREIGN KEY (\`gender_id\`) REFERENCES \`genders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`movies\` ADD CONSTRAINT \`FK_cf3f214b7bdb6bc5e641930ee6b\` FOREIGN KEY (\`genre_id\`) REFERENCES \`genres\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`directors\` ADD CONSTRAINT \`FK_54fc36b9cd040c6bb9549cef176\` FOREIGN KEY (\`gender_id\`) REFERENCES \`genders\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`votes\` ADD CONSTRAINT \`FK_bd941f763560c3c4400366467f8\` FOREIGN KEY (\`movie_id\`) REFERENCES \`movies\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`votes\` ADD CONSTRAINT \`FK_27be2cab62274f6876ad6a31641\` FOREIGN KEY (\`user_id\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`movies_actors_actors\` ADD CONSTRAINT \`FK_638b1d6f6929495fa5b87206daf\` FOREIGN KEY (\`moviesId\`) REFERENCES \`movies\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`movies_actors_actors\` ADD CONSTRAINT \`FK_6f9bbef3136f7efc40a5a55886c\` FOREIGN KEY (\`actorsId\`) REFERENCES \`actors\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`movies_directors_directors\` ADD CONSTRAINT \`FK_668ef403881ed50222bd69d60c9\` FOREIGN KEY (\`moviesId\`) REFERENCES \`movies\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`movies_directors_directors\` ADD CONSTRAINT \`FK_bec316d1f8c152edabafd5ad4e2\` FOREIGN KEY (\`directorsId\`) REFERENCES \`directors\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`movies_directors_directors\` DROP FOREIGN KEY \`FK_bec316d1f8c152edabafd5ad4e2\``);
        await queryRunner.query(`ALTER TABLE \`movies_directors_directors\` DROP FOREIGN KEY \`FK_668ef403881ed50222bd69d60c9\``);
        await queryRunner.query(`ALTER TABLE \`movies_actors_actors\` DROP FOREIGN KEY \`FK_6f9bbef3136f7efc40a5a55886c\``);
        await queryRunner.query(`ALTER TABLE \`movies_actors_actors\` DROP FOREIGN KEY \`FK_638b1d6f6929495fa5b87206daf\``);
        await queryRunner.query(`ALTER TABLE \`votes\` DROP FOREIGN KEY \`FK_27be2cab62274f6876ad6a31641\``);
        await queryRunner.query(`ALTER TABLE \`votes\` DROP FOREIGN KEY \`FK_bd941f763560c3c4400366467f8\``);
        await queryRunner.query(`ALTER TABLE \`directors\` DROP FOREIGN KEY \`FK_54fc36b9cd040c6bb9549cef176\``);
        await queryRunner.query(`ALTER TABLE \`movies\` DROP FOREIGN KEY \`FK_cf3f214b7bdb6bc5e641930ee6b\``);
        await queryRunner.query(`ALTER TABLE \`actors\` DROP FOREIGN KEY \`FK_10c7c6cb20ef75e11f1050a8018\``);
        await queryRunner.query(`ALTER TABLE \`users\` DROP FOREIGN KEY \`FK_7fc7be0985c48fa965c80fc8775\``);
        await queryRunner.query(`ALTER TABLE \`users\` DROP FOREIGN KEY \`FK_a2cecd1a3531c0b041e29ba46e1\``);
        await queryRunner.query(`DROP INDEX \`IDX_bec316d1f8c152edabafd5ad4e\` ON \`movies_directors_directors\``);
        await queryRunner.query(`DROP INDEX \`IDX_668ef403881ed50222bd69d60c\` ON \`movies_directors_directors\``);
        await queryRunner.query(`DROP TABLE \`movies_directors_directors\``);
        await queryRunner.query(`DROP INDEX \`IDX_6f9bbef3136f7efc40a5a55886\` ON \`movies_actors_actors\``);
        await queryRunner.query(`DROP INDEX \`IDX_638b1d6f6929495fa5b87206da\` ON \`movies_actors_actors\``);
        await queryRunner.query(`DROP TABLE \`movies_actors_actors\``);
        await queryRunner.query(`DROP TABLE \`votes\``);
        await queryRunner.query(`DROP TABLE \`directors\``);
        await queryRunner.query(`DROP TABLE \`movies\``);
        await queryRunner.query(`DROP TABLE \`actors\``);
        await queryRunner.query(`DROP TABLE \`users\``);
        await queryRunner.query(`DROP TABLE \`roles\``);
        await queryRunner.query(`DROP TABLE \`genres\``);
        await queryRunner.query(`DROP TABLE \`genders\``);
    }

}
