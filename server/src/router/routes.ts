import {
	DirectorController,
	RoleController,
	VoteController,
	ActorController,
	UserController,
	GenderController,
	GenreController,
	MovieController,
	S3Controller,
} from "../controllers/index";

import { BASE_PATH } from "../configs/enviroment";

import { UserRole } from "../types/enums";

const routesData = {
	gender: [
		{
			method: "get",
			route: `/genders`,
			controller: GenderController,
			action: "all",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "get",
			route: `/genders/:id`,
			controller: GenderController,
			action: "one",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "post",
			route: `/genders`,
			controller: GenderController,
			action: "save",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "patch",
			route: `/genders/:id`,
			controller: GenderController,
			action: "update",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "delete",
			route: `/genders/:id`,
			controller: GenderController,
			action: "remove",
			rolesAllowed: [UserRole.ADMIN],
		},
	],
	genre: [
		{
			method: "get",
			route: `/genres`,
			controller: GenreController,
			action: "all",
			unauthenticated: true,
		},
		{
			method: "get",
			route: `/genres/:id`,
			controller: GenreController,
			action: "one",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "post",
			route: `/genres`,
			controller: GenreController,
			action: "save",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "patch",
			route: `/genres/:id`,
			controller: GenreController,
			action: "update",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "delete",
			route: `/genres/:id`,
			controller: GenreController,
			action: "remove",
			rolesAllowed: [UserRole.ADMIN],
		},
	],
	actor: [
		{
			method: "get",
			route: `/actors`,
			controller: ActorController,
			action: "all",
			unauthenticated: true,
		},
		{
			method: "get",
			route: `/actors/:id`,
			controller: ActorController,
			action: "one",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "post",
			route: `/actors`,
			controller: ActorController,
			action: "save",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "patch",
			route: `/actors/:id`,
			controller: ActorController,
			action: "update",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "delete",
			route: `/actors/:id`,
			controller: ActorController,
			action: "remove",
			rolesAllowed: [UserRole.ADMIN],
		},
	],
	movie: [
		{
			method: "get",
			route: `/movies`,
			controller: MovieController,
			action: "all",
			unauthenticated: true,
		},
		{
			method: "get",
			route: `/movies/:id`,
			controller: MovieController,
			action: "one",
			unauthenticated: true,
		},
		{
			method: "post",
			route: `/movies`,
			controller: MovieController,
			action: "save",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "patch",
			route: `/movies/:id`,
			controller: MovieController,
			action: "update",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "delete",
			route: `/movies/:id`,
			controller: MovieController,
			action: "remove",
			rolesAllowed: [UserRole.ADMIN],
		},
	],
	vote: [
		{
			method: "post",
			route: `/votes`,
			controller: VoteController,
			action: "saveVote",
			rolesAllowed: [UserRole.USER, UserRole.ADMIN],
		},
	],
	user: [
		{
			method: "get",
			route: `/users`,
			controller: UserController,
			action: "all",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "get",
			route: `/users/deauthenticate`,
			controller: UserController,
			action: "deauthenticate",
			rolesAllowed: [UserRole.USER, UserRole.ADMIN],
		},
		{
			method: "get",
			route: "/users/role",
			controller: UserController,
			action: "getRole",
			rolesAllowed: [UserRole.USER, UserRole.ADMIN],
		},
		{
			method: "get",
			route: `/users/:id`,
			controller: UserController,
			action: "one",
			rolesAllowed: [UserRole.ADMIN],
		},

		{
			method: "post",
			route: `/users`,
			controller: UserController,
			action: "save",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "post",
			route: `/users/activate/:id`,
			controller: UserController,
			action: "enable",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "post",
			route: `/users/authenticate`,
			controller: UserController,
			action: "auhenticate",
			unauthenticated: true,
		},
		{
			method: "patch",
			route: `/users/:id`,
			controller: UserController,
			action: "update",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "delete",
			route: `/users/:id`,
			controller: UserController,
			action: "remove",
			rolesAllowed: [UserRole.ADMIN],
		},
	],
	role: [
		{
			method: "get",
			route: `/roles`,
			controller: RoleController,
			action: "all",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "get",
			route: `/roles/:id`,
			controller: RoleController,
			action: "one",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "post",
			route: `/roles`,
			controller: RoleController,
			action: "save",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "patch",
			route: `/roles/:id`,
			controller: RoleController,
			action: "update",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "delete",
			route: `/roles/:id`,
			controller: RoleController,
			action: "remove",
			rolesAllowed: [UserRole.ADMIN],
		},
	],
	director: [
		{
			method: "get",
			route: `/directors`,
			controller: DirectorController,
			action: "all",
			unauthenticated: true,
		},
		{
			method: "get",
			route: `/directors/:id`,
			controller: DirectorController,
			action: "one",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "post",
			route: `/directors`,
			controller: DirectorController,
			action: "save",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "patch",
			route: `/directors/:id`,
			controller: DirectorController,
			action: "update",
			rolesAllowed: [UserRole.ADMIN],
		},
		{
			method: "delete",
			route: `/directors/:id`,
			controller: DirectorController,
			action: "remove",
			rolesAllowed: [UserRole.ADMIN],
		},
	],
	s3: [
		{
			method: "post",
			route: `/upload`,
			controller: S3Controller,
			action: "upload",
			rolesAllowed: [UserRole.ADMIN],
		},
	],
};

const routesKey = Object.keys(routesData);
const flatedRoutes = routesKey.flatMap((key) => routesData[key]);
const authenticatedRotes = flatedRoutes.filter((route) => !route.unauthenticated);
const unauthenticatedRoutes = flatedRoutes.filter((route) => route.unauthenticated);

const formatPath = (path: string) => {
	const formatedPath = BASE_PATH + path.replace("/:id", "");

	return formatedPath;
};

export const fetchRolesAllowedByRotePathAndMethod = (path: string, method: string) => {
	const route = flatedRoutes.find(
		(route) => formatPath(route.route) === path && route.method === method.toLowerCase()
	);

	return route?.rolesAllowed;
};

export const Routes = {
	authenticated: authenticatedRotes,
	unauthenticated: unauthenticatedRoutes,
	routes: flatedRoutes,
};
