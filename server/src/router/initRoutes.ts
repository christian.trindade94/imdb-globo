import { Request, Response, NextFunction, Express } from "express";

import { IRoutes } from "../types/types";
import { BASE_PATH } from "../configs/enviroment";

export const initRoutes = (app: Express, routes: IRoutes) => {
	routes.forEach((route) => {
		(app as Express)[route.method](
			BASE_PATH + route.route,
			async (req: Request, res: Response, next: NextFunction) => {
				const result = await new (route.controller as any)()[route.action](req, res, next);
				if (result instanceof Promise) {
					result ? res.send(result) : undefined;
				} else if (result) {
					res.json(result);
				}
			}
		);
	});
};
