import { NextFunction, Request, Response } from "express";

export const errorHandler = async (
	err: Error,
	request: Request,
	response: Response,
	next: NextFunction
) => {
	response.status(500).send("Something went wrong!");

	console.error(err);

	next();
};
