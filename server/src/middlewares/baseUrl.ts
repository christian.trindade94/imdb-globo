import { Response, Request, NextFunction } from "express";

export const putBaseUrl = (request: Request, response: Response, next: NextFunction) => {
	const path = request.path;
	const pathWithoutParams = path.replace(/\/[0-9a-fA-F]+$/, "");
	request.baseUrl = pathWithoutParams;

	next();
};
