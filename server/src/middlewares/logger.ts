import fs from "fs";
import morgan from "morgan";
import path from "path";

import { LOG_DIRECTORY } from "../configs/enviroment";

if (!fs.existsSync(LOG_DIRECTORY)) {
	fs.mkdirSync(LOG_DIRECTORY);
}

const logFilename = path.join(LOG_DIRECTORY, "access.log");
const accessLogStream = fs.createWriteStream(logFilename, { flags: "a" });

export const logger = morgan("combined", { stream: accessLogStream });
