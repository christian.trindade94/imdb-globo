import { Response, Request, NextFunction } from "express";
import { promisify } from "util";
import jwt from "jsonwebtoken";

const jwtVerify = promisify(jwt.verify);

import { SECRET, REFRESH_SECRET, COOKIE_SECURE } from "../configs/enviroment";

import { fetchRolesAllowedByRotePathAndMethod } from "../router/routes";

const isAuthorized = (role: number, baseUrl: string, method: string) => {
	const rolesAllowed = fetchRolesAllowedByRotePathAndMethod(baseUrl, method);
	return rolesAllowed.includes(role);
};

const refresh = async (request, response: Response) => {
	const { refreshToken } = request.cookies;

	try {
		const decoded = await jwtVerify(refreshToken, REFRESH_SECRET);

		delete decoded.expiresIn;
		delete decoded.exp;
		const token = jwt.sign({ ...decoded }, SECRET, { expiresIn: "24h" });

		response.cookie("token", token, {
			httpOnly: true,
			secure: COOKIE_SECURE,
			sameSite: COOKIE_SECURE ? "none" : "lax",
			maxAge: 24 * 60 * 60 * 1000,
		});
	} catch (error) {
		return response.status(401).json({ message: "unauthorized access" });
	}
};

export const authenticate = async (request: Request, response: Response, next: NextFunction) => {
	const { token } = request.cookies;

	if (!token) {
		return response.status(401).json({ message: "unauthorized access" });
	}

	try {
		const decoded = await jwtVerify(token, SECRET);

		request["user"] = decoded;

		const resourceAuthorized = isAuthorized(
			request["user"].role,
			request.baseUrl,
			request.method
		);

		if (!resourceAuthorized) {
			return response.status(401).json({ message: "unauthorized access" });
		}

		await refresh(request, response);

		next();
	} catch {
		return response.status(401).json({ message: "unauthorized access" });
	}
};
