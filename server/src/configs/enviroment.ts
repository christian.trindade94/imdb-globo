import "dotenv/config";

export const PORT = process.env.PORT;

export const DB_USERNAME = process.env.MYSQL_USER;
export const DB_NAME = process.env.MYSQL_DATABASE;
export const DB_PASSWORD = process.env.MYSQL_PASSWORD;
export const DB_PORT = parseInt(process.env.MYSQL_PORT as string);

export const S3_BUCKET = process.env.S3_BUCKET;
export const S3_SECRET_KEY = process.env.S3_SECRET_KEY;
export const S3_ACCESS_KEY = process.env.S3_ACCESS_KEY;

export const SECRET_PASS = process.env.SECRET_PASS;
export const SECRET = process.env.JWT_SECRET;
export const REFRESH_SECRET = process.env.JWT_REFRESH_SECRET;
export const COOKIE_SECURE = process.env.COOKIE_SECURE === "true";

export const LOG_DIRECTORY = process.env.LOG_FILEPATH;
export const BASE_PATH = process.env.BASE_PATH;
