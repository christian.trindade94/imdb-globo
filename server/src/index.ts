import { PORT } from "./configs/enviroment";

import app from "./app";
import { initializeDatabase } from "./initializeDatabase";

const startApp = (async () => {
	await initializeDatabase();
	app.listen(PORT, () => {
		console.log(`Express server has started on port ${PORT}. Open http://localhost:${PORT}`);
	});
})();
