import { Request } from "express";

import { UserRole } from "./enums";

export interface IMDBRequest extends Request {
	user: {
		id: number;
		email: string;
		role: number;
	};
	files?: {
		file: {
			name: string;
			data: Buffer | Uint8Array | string;
		};
	};
}

export type IRoutes = IRoute[];

export type IRoute = {
	method: string;
	route: string;
	controller: any;
	action: string;
	rolesAllowed: UserRole[];
};
