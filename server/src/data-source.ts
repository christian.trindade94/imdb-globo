import "reflect-metadata";
import { DataSource } from "typeorm";

import { DB_USERNAME, DB_NAME, DB_PASSWORD, DB_PORT } from "./configs/enviroment";

export const AppDataSource = new DataSource({
	type: "mysql",
	host: "localhost",
	port: DB_PORT,
	username: DB_USERNAME,
	password: DB_PASSWORD,
	database: DB_NAME,
	synchronize: true,
	logging: false,
	entities: ["src/entities/**/*.ts"],
	migrations: ["src/migrations/**/*.ts"],
	subscribers: ["src/subscribers/**/*.ts"],
	migrationsTableName: "migrations",
});
