import { AppDataSource } from "./data-source";

export const initializeDatabase = async () => {
	try {
		await AppDataSource.initialize();
	} catch (error) {
		console.error("Erro at conect with database:", error);
		throw error;
	}
};
