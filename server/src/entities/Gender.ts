import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	BaseEntity,
	CreateDateColumn,
	UpdateDateColumn,
} from "typeorm";

import { Length } from "class-validator";

@Entity("genders")
export class Gender extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	@Length(1, 30)
	label: string;

	@CreateDateColumn()
	createdDate: Date;

	@UpdateDateColumn()
	updatedDate: Date;
}
