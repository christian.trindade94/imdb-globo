import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	BaseEntity,
	CreateDateColumn,
	UpdateDateColumn,
	JoinColumn,
	JoinTable,
	ManyToOne,
	ManyToMany,
} from "typeorm";

import { Length, IsNotEmpty, IsUrl, IsInt, Min, Max } from "class-validator";

import { Genre } from "./Genre";
import { Actor } from "./Actor";
import { Director } from "./Director";

@Entity("movies")
export class Movie extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	@Length(2, 100)
	name: string;

	@Column({ type: "text" })
	@IsNotEmpty()
	description: string;

	@Column()
	@IsUrl()
	photo: string;

	@Column()
	@IsInt()
	@Min(1)
	genre_id: number;

	@Column({ type: "float" })
	@Min(0)
	@Max(4)
	average_vote_score: number;

	@ManyToOne(() => Genre)
	@JoinColumn({ name: "genre_id" })
	gender: Genre;

	@ManyToMany(() => Actor)
	@JoinTable()
	actors: Actor[];

	@ManyToMany(() => Director)
	@JoinTable()
	directors: Director[];

	@CreateDateColumn()
	createdDate: Date;

	@UpdateDateColumn()
	updatedDate: Date;
}
