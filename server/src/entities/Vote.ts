import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	BaseEntity,
	CreateDateColumn,
	UpdateDateColumn,
	ManyToOne,
	JoinColumn,
} from "typeorm";

import { Min, Max, IsInt } from "class-validator";

import { Movie } from "./Movie";
import { User } from "./User";

@Entity("votes")
export class Vote extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	@IsInt()
	@Min(1)
	user_id: number;

	@Column()
	@IsInt()
	@Min(1)
	movie_id: number;

	@Column({ type: "float" })
	@Min(0)
	@Max(4)
	score: number;

	@ManyToOne(() => Movie, { onDelete: "CASCADE", onUpdate: "CASCADE" })
	@JoinColumn({ name: "movie_id" })
	movie: Movie;

	@ManyToOne(() => User)
	@JoinColumn({ name: "user_id" })
	user: User;

	@CreateDateColumn()
	createdDate: Date;

	@UpdateDateColumn()
	updatedDate: Date;
}
