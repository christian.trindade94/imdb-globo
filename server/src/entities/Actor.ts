import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	BaseEntity,
	CreateDateColumn,
	UpdateDateColumn,
	JoinColumn,
	ManyToOne,
	ManyToMany,
} from "typeorm";

import { Length, IsNotEmpty, IsUrl, IsInt, Min } from "class-validator";

import { Gender } from "./Gender";
import { Movie } from "./Movie";

@Entity("actors")
export class Actor extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	@Length(2, 100)
	name: string;

	@Column()
	@Length(2, 100)
	lastname: string;

	@Column({ type: "text" })
	@IsNotEmpty()
	description: string;

	@Column()
	@IsUrl()
	photo: string;

	@Column()
	@IsInt()
	@Min(1)
	gender_id: number;

	@ManyToOne(() => Gender)
	@JoinColumn({ name: "gender_id" })
	gender: Gender;

	@ManyToMany(() => Movie)
	movies: Movie[];

	@CreateDateColumn()
	createdDate: Date;

	@UpdateDateColumn()
	updatedDate: Date;
}
