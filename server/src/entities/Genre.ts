import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	BaseEntity,
	CreateDateColumn,
	UpdateDateColumn,
} from "typeorm";

import { Length } from "class-validator";

@Entity("genres")
export class Genre extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	@Length(1, 40)
	label: string;

	@CreateDateColumn()
	createdDate: Date;

	@UpdateDateColumn()
	updatedDate: Date;
}
