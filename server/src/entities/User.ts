import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	BaseEntity,
	CreateDateColumn,
	UpdateDateColumn,
	JoinColumn,
	ManyToOne,
} from "typeorm";

import { Length, IsUrl, IsInt, Min, IsEmail, IsBoolean } from "class-validator";

import { Gender } from "./Gender";
import { Role } from "./Role";

@Entity("users")
export class User extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	@Length(2, 100)
	name: string;

	@Column()
	@Length(2, 100)
	lastname: string;

	@Column()
	@Length(2, 100)
	username: string;

	@Column({ select: false })
	@Length(8, 100)
	password: string;

	@Column()
	@IsInt()
	@Min(1)
	role_id: string;

	@ManyToOne(() => Role)
	@JoinColumn({ name: "role_id" })
	role: Role;

	@Column()
	@IsEmail()
	email: string;

	@Column()
	@IsUrl()
	photo: string;

	@Column()
	@IsInt()
	@Min(1)
	gender_id: number;

	@Column({ default: false })
	@IsBoolean()
	disabled: boolean;

	@ManyToOne(() => Gender)
	@JoinColumn({ name: "gender_id" })
	gender: Gender;

	@CreateDateColumn()
	createdDate: Date;

	@UpdateDateColumn()
	updatedDate: Date;
}
