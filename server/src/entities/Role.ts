import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	BaseEntity,
	CreateDateColumn,
	UpdateDateColumn,
} from "typeorm";

import { Length } from "class-validator";

@Entity("roles")
export class Role extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	@Length(1, 20)
	label: string;

	@CreateDateColumn()
	createdDate: Date;

	@UpdateDateColumn()
	updatedDate: Date;
}
