import request from "supertest";

import app from "../../app";
import { initializeDatabase } from "../../initializeDatabase";

import { PORT } from "../../configs/enviroment";

const routes = {
	authenticate: "/api/users/authenticate",
	deauthenticate: "/api/users/deauthenticate",
	genders: "/api/genders",
};

const adminCredentials = {
	password: "password123",
	email: "admin@imdgbglobo.com",
};

const userCredentials = {
	password: "password123",
	email: "user@imdgbglobo.com",
};

const invalidCredentials = {
	password: "XXXXXXXXXXX",
	email: "admin@imdgbglobo.com",
};

beforeAll(async () => {
	await initializeDatabase();
	app.listen(PORT, () => {
		console.log(`Express server has started on port ${PORT}. Open http://localhost:${PORT}`);
	});
});

describe("Authentication and Deauthentication Tests", () => {
	let authCookies: string;

	it("should authenticate a normal user and return status code 200", async () => {
		const res = await request(app).post(routes.authenticate).send(userCredentials);

		expect(res.statusCode).toEqual(200);
		expect(res.body).toHaveProperty("id");

		authCookies = res.headers["set-cookie"];
	});

	it("should try to access a resource not allowed for a normal user and return 401", async () => {
		const res = await request(app).get(routes.genders);

		expect(res.statusCode).toEqual(401);
	});

	it("should authenticate admin user and return status code 200", async () => {
		const res = await request(app).post(routes.authenticate).send(adminCredentials);

		expect(res.statusCode).toEqual(200);
		expect(res.body).toHaveProperty("id");

		authCookies = res.headers["set-cookie"];
	});

	it("should try to access a resource allowed for a admin user and return 200", async () => {
		const res = await request(app).get(routes.genders).set("Cookie", authCookies);

		expect(res.statusCode).toEqual(200);
	});

	it("should deauthenticate user and return status code 200", async () => {
		const res = await request(app).get(routes.deauthenticate).set("Cookie", authCookies);

		expect(res.statusCode).toEqual(200);
	});

	it("should return unauthorized for invalid credentials", async () => {
		const res = await request(app).post(routes.authenticate).send(invalidCredentials);

		expect(res.statusCode).toEqual(401);
	});
});

describe("Gender CRUD Tests", () => {
	let authCookies: string;
	let genderId: number;

	beforeAll(async () => {
		const res = await request(app).post(routes.authenticate).send(adminCredentials);

		expect(res.statusCode).toEqual(200);
		expect(res.body).toHaveProperty("id");

		authCookies = res.headers["set-cookie"];
	});

	it("should list a gender of genders", async () => {
		const res = await request(app).get(routes.genders).set("Cookie", authCookies);

		expect(res.statusCode).toEqual(200);
		expect(res.body.length).toBeGreaterThan(0);
	});

	it("should create a gender with wrong label format and return a status 400", async () => {
		const genderData = {
			label: "ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd",
		};
		const res = await request(app)
			.post(routes.genders)
			.set("Cookie", authCookies)
			.send(genderData);

		expect(res.statusCode).toEqual(400);
	});

	it("should create a gender and return status 200 and a id", async () => {
		const genderData = {
			label: "test",
		};

		const res = await request(app)
			.post(routes.genders)
			.set("Cookie", authCookies)
			.send(genderData);

		expect(res.statusCode).toEqual(200);
		expect(res.body).toHaveProperty("id");
		genderId = res.body.id;
	});

	it("should update a gender and return status 200", async () => {
		const genderData = {
			label: "test2",
		};

		const res = await request(app)
			.patch(`${routes.genders}/${genderId}`)
			.set("Cookie", authCookies)
			.send(genderData);

		expect(res.statusCode).toEqual(200);
		expect(res.body.label).toEqual("test2");
	});

	it("shound delete a gender and return a status 200", async () => {
		const res = await request(app)
			.delete(`${routes.genders}/${genderId}`)
			.set("Cookie", authCookies);

		expect(res.statusCode).toEqual(200);
	});

	it("should try to access a genre that doesn't exist and return status 404", async () => {
		const res = await request(app)
			.get(`${routes.genders}/${genderId}`)
			.set("Cookie", authCookies);

		expect(res.statusCode).toEqual(404);
	});
});
