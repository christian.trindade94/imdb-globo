import { EntitySubscriberInterface, EventSubscriber, InsertEvent } from "typeorm";

import { Vote } from "../entities/Vote";

import { VoteController, MovieController } from "../controllers";

@EventSubscriber()
export class VoteSubscriber implements EntitySubscriberInterface<Vote> {
	listenTo() {
		return Vote;
	}

	async beforeInsert(event: InsertEvent<Vote>) {
		const record = event.entity;
		const result = await VoteController.FetchVotesCountByMovieId(record.movie_id);
		let { totalScore, totalCount } = result;

		totalScore = Number(totalScore);
		totalCount = Number(totalCount);

		const movieAverageScore = (totalScore + record.score) / (totalCount + 1);

		await MovieController.UpdateAverageScore(record.movie_id, movieAverageScore);
	}
}
