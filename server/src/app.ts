import express from "express";
import session from "express-session";
import cookieParser from "cookie-parser";
import upload from "express-fileupload";

import cors from "cors";
import bodyParser from "body-parser";
import helmet from "helmet";
import swaggerUi from "swagger-ui-express";

import "express-async-errors";

const swaggerJson = require("./configs/swagger.json");

import { SECRET_PASS } from "./configs/enviroment";

import { Routes } from "./router/routes";
import { initRoutes } from "./router/initRoutes";

import { putBaseUrl } from "./middlewares/baseUrl";
import { authenticate } from "./middlewares/authenticate";
import { logger } from "./middlewares/logger";
import { errorHandler } from "./middlewares/error";

const app = express();

app.use(helmet());
app.use(upload());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(session({ secret: SECRET_PASS, resave: false, saveUninitialized: false }));
app.use(logger);
app.use(putBaseUrl);

app.use(
	cors({
		origin: true,
		credentials: true,
	})
);

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerJson));

initRoutes(app, Routes.unauthenticated);
app.use(authenticate);
initRoutes(app, Routes.authenticated);

app.use(errorHandler);

export default app;
