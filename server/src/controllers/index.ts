import { ActorController } from "./ActorController";
import { MovieController } from "./MovieController";
import { UserController } from "./UserController";
import { GenderController } from "./GenderController";
import { GenreController } from "./GenreController";
import { DirectorController } from "./DirectorController";
import { RoleController } from "./RoleController";
import { VoteController } from "./VoteController";
import { S3Controller } from "./S3Controller";

export {
	DirectorController,
	RoleController,
	VoteController,
	ActorController,
	UserController,
	GenderController,
	GenreController,
	MovieController,
	S3Controller,
};
