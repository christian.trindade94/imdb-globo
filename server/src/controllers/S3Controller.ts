import { Response, Request } from "express";
import { v4 as uuidv4 } from "uuid";
import AWS from "aws-sdk";

import { IMDBRequest } from "../types/types";

import { S3_BUCKET, S3_SECRET_KEY, S3_ACCESS_KEY } from "../configs/enviroment";

export class S3Controller {
	s3 = new AWS.S3({
		accessKeyId: S3_ACCESS_KEY,
		secretAccessKey: S3_SECRET_KEY,
	});

	async upload(request: IMDBRequest, response: Response) {
		const file = request?.files?.file;

		if (!file) {
			return response.status(400).send("No file uploaded");
		}

		const fileUUID = uuidv4();

		const params = {
			Bucket: S3_BUCKET,
			Key: `${fileUUID}-${file.name}`,
			Body: file.data,
		};
		const data = await this.s3.upload(params).promise();

		return { url: data.Location };
	}
}
