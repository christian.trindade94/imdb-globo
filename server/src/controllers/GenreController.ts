import { AppDataSource } from "../data-source";

import { CRUDController } from "./CrudController";

import { Genre } from "../entities/Genre";

export class GenreController extends CRUDController {
	constructor() {
		super();
		this.entityClass = Genre;
		this.entityName = "Genre";
		this.repository = AppDataSource.getRepository(Genre);
	}
}
