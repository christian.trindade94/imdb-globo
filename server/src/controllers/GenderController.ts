import { AppDataSource } from "../data-source";

import { CRUDController } from "./CrudController";

import { Gender } from "../entities/Gender";

export class GenderController extends CRUDController {
	constructor() {
		super();
		this.entityClass = Gender;
		this.entityName = "Gender";
		this.repository = AppDataSource.getRepository(Gender);
	}
}
