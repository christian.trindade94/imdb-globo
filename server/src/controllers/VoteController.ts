import { Response } from "express";

import { AppDataSource } from "../data-source";

import { CRUDController } from "./CrudController";

import { Vote } from "../entities/Vote";

import { IMDBRequest } from "../types/types";

export class VoteController extends CRUDController {
	constructor() {
		super();

		this.entityClass = Vote;
		this.entityName = "Vote";
		this.repository = AppDataSource.getRepository(Vote);
	}

	static async FetchVotesCountByMovieId(movieId: number) {
		const repository = AppDataSource.getRepository(Vote);

		return repository
			.createQueryBuilder("vote")
			.select(["COUNT(vote.id) as totalCount", "SUM(vote.score) as totalScore"])
			.where("vote.movie_id = :movieId", { movieId })
			.getRawOne();
	}

	async saveVote(request: Request & IMDBRequest, response: Response) {
		const data = request.body;
		const userId = request.user.id;
		data.user_id = userId;

		const newRecord = Object.assign(new this.entityClass(), data);

		const validateErrors = await this.validate(newRecord);

		if (validateErrors) {
			response.status(400).send(validateErrors);
			return;
		}

		return this.saveRecord(newRecord);
	}
}
