import { Response, Request } from "express";

import { AppDataSource } from "../data-source";

import { Movie } from "../entities/Movie";

import { CRUDController } from "./CrudController";

export class MovieController extends CRUDController {
	constructor() {
		super();
		this.relations = {
			gender: true,
			actors: true,
			directors: true,
		};
		this.entityClass = Movie;
		this.entityName = "Movie";
		this.repository = AppDataSource.getRepository(Movie);
	}

	private formatArrayQuery = (param: string) => {
		try {
			return param.split(",").map((id: string) => parseInt(id.trim(), 10));
		} catch {
			return [];
		}
	};

	async all(request: Request, response: Response) {
		const { name, genre, actors, directors, page, limit } = request.query;
		const queryBuilder = this.repository.createQueryBuilder("movie");

		if (name) {
			queryBuilder.andWhere("movie.name LIKE :name", { name: `%${name}%` });
		}

		if (genre) {
			queryBuilder.andWhere("movie.genre_id = :genre", { genre });
		}

		if (actors) {
			const actorIds = this.formatArrayQuery(actors as string);
			queryBuilder.innerJoinAndSelect(
				"movie.actors",
				"filteredActor",
				"filteredActor.id IN (:...actorIds)",
				{ actorIds }
			);
		}

		if (directors) {
			const directorIds = this.formatArrayQuery(directors as string);
			queryBuilder.innerJoinAndSelect(
				"movie.directors",
				"filteredDirector",
				"filteredDirector.id IN (:...directorIds)",
				{ directorIds }
			);
		}

		if (page && limit) {
			const pageInt = parseInt(page as string);
			const limitInt = parseInt(limit as string);

			if (pageInt && limitInt) {
				queryBuilder.skip((pageInt - 1) * limitInt).take(limit);
			}
		}

		const movies = await queryBuilder.getMany();

		return movies;
	}

	static async UpdateAverageScore(movieId: number, averageScore: number) {
		const movieRepository = AppDataSource.getRepository(Movie);

		const movie = await movieRepository.findOneBy({ id: movieId });
		movie.average_vote_score = averageScore;
		await movieRepository.save(movie);
	}
}
