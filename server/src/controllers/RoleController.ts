import { AppDataSource } from "../data-source";

import { CRUDController } from "./CrudController";

import { Role } from "../entities/Role";

export class RoleController extends CRUDController {
	constructor() {
		super();

		this.entityClass = Role;
		this.entityName = "Role";
		this.repository = AppDataSource.getRepository(Role);
	}
}
