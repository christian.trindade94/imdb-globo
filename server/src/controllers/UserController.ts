import { Response, Request } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import { IMDBRequest } from "../types/types";

import { AppDataSource } from "../data-source";

import { CRUDController } from "./CrudController";

import { User } from "../entities/User";

import { SECRET, REFRESH_SECRET, COOKIE_SECURE } from "../configs/enviroment";

export class UserController extends CRUDController {
	constructor() {
		super();
		this.relations = {
			gender: true,
			role: true,
		};
		this.logicalDelete = true;
		this.entityClass = User;
		this.entityName = "User";
		this.repository = AppDataSource.getRepository(User);
	}

	async deauthenticate(request: Request, response: Response) {
		response.clearCookie("token");
		response.clearCookie("refreshToken");
		response.status(200).json({ message: "Logout" });
	}

	async auhenticate(request: Request, response: Response) {
		const { email, password } = request.body;

		const user = await this.repository.findOne({
			where: { email },
			select: ["id", "email", "password", "disabled", "role_id"],
		});

		if (!user || user.disabled) {
			this.notFound(response);
			return;
		}

		const passwordMatch = await bcrypt.compare(password, user.password);

		if (!passwordMatch) {
			response.status(401).json({ message: "incorrect email or password" });
			return;
		}

		const payload = {
			id: user.id,
			email: user.email,
			role: user.role_id,
		};

		const token = jwt.sign(payload, SECRET, { expiresIn: "24h" });
		const refreshToken = jwt.sign(payload, REFRESH_SECRET, { expiresIn: "7d" });

		response.cookie("refreshToken", refreshToken, {
			httpOnly: true,
			secure: COOKIE_SECURE,
			sameSite: COOKIE_SECURE ? "none" : "lax",
			maxAge: 7 * 24 * 60 * 60 * 1000,
		});

		response.cookie("token", token, {
			httpOnly: true,
			secure: COOKIE_SECURE,
			sameSite: COOKIE_SECURE ? "none" : "lax",
			maxAge: 24 * 60 * 60 * 1000,
		});

		response.status(200).json({ ...payload });
	}

	async save(request: Request, response: Response) {
		const data = request.body;

		const newRecord = Object.assign(new this.entityClass(), data);

		const validateErrors = await this.validate(newRecord);

		if (validateErrors) {
			response.status(400).send(validateErrors);
			return;
		}

		const userFound = await this.repository.findOne({
			where: { email: data.email },
		});

		if (userFound) {
			response.status(400).json({ message: "This email is already being used" });
			return;
		}

		const saltRounds = 10;
		const passwordHash = await bcrypt.hash(data.password, saltRounds);

		newRecord.password = passwordHash;

		return this.repository.save(newRecord);
	}

	async enable(request: Request, response: Response) {
		const id = parseInt(request.params.id);

		const record = await this.repository.findOneBy({ id });

		if (!record) {
			this.notFound(response);
			return;
		}

		record.disabled = false;

		return this.repository.save(record);
	}

	async getRole(request: IMDBRequest, response: Response) {
		const userData = request.user;
		response.status(200).json({ role: userData.role });
	}
}
