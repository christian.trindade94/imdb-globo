import { Response, Request } from "express";
import { validateOrReject } from "class-validator";

export class CRUDController {
	logicalDelete: boolean;
	relations: { [key: string]: boolean };
	entityName: string;
	entityClass;
	repository;

	fetchRecord(id: number) {
		return this.repository.findOne({
			where: { id },
			relations: this.relations || {},
		});
	}

	saveRecord(data) {
		return this.repository.save(data);
	}

	deleteRecord(data) {
		return this.repository.remove(data);
	}

	notFoundMessage() {
		const message = this.entityName + " not found";
		return { message };
	}

	notFound(response: Response) {
		const message = this.notFoundMessage();
		response.status(404).json(message);
	}

	removed() {
		const message = this.entityName + " removed";
		return { message };
	}

	async validate(input) {
		try {
			await validateOrReject(input);
		} catch (errors) {
			return errors;
		}
	}

	async all(request: Request, response: Response) {
		return this.repository.find({ relations: this.relations || {} });
	}

	async one(request: Request, response: Response) {
		const id = parseInt(request.params.id);

		const result = await this.fetchRecord(id);

		if (!result) {
			this.notFound(response);
			return;
		}

		return result;
	}

	async save(request: Request, response: Response) {
		const data = request.body;

		const newRecord = Object.assign(new this.entityClass(), data);

		const validateErrors = await this.validate(newRecord);

		if (validateErrors) {
			response.status(400).send(validateErrors);
			return;
		}

		return this.saveRecord(newRecord);
	}

	async update(request: Request, response: Response) {
		const id = parseInt(request.params.id);
		const data = request.body;

		const record = await this.repository.findOne({ where: { id } });

		if (!record) {
			this.notFound(response);
			return;
		}

		const newRecord = { ...record, ...data };

		const validateErrors = await this.validate(newRecord);
		if (validateErrors) {
			response.status(400).send(validateErrors);
			return;
		}

		return this.saveRecord(newRecord);
	}

	async remove(request: Request, response: Response) {
		const id = parseInt(request.params.id);

		const result = await this.fetchRecord(id);

		if (!result) {
			this.notFound(response);
			return;
		}

		if (this.logicalDelete) {
			result.disabled = true;
			await this.saveRecord(result);
		} else {
			await this.deleteRecord(result);
		}

		return this.removed();
	}
}
