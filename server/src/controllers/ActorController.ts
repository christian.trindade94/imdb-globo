import { AppDataSource } from "../data-source";

import { CRUDController } from "./CrudController";

import { Actor } from "../entities/Actor";

export class ActorController extends CRUDController {
	constructor() {
		super();
		this.relations = {
			gender: true,
		};
		this.entityClass = Actor;
		this.entityName = "Actor";
		this.repository = AppDataSource.getRepository(Actor);
	}
}
