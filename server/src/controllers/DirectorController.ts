import { AppDataSource } from "../data-source";

import { CRUDController } from "./CrudController";

import { Director } from "../entities/Director";

export class DirectorController extends CRUDController {
	constructor() {
		super();
		this.relations = {
			gender: true,
		};
		this.entityClass = Director;
		this.entityName = "Director";
		this.repository = AppDataSource.getRepository(Director);
	}
}
