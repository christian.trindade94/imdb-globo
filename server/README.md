# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Run `docker-compose up` command
3. Run `npm start` command

# use Migrations to update the database

Create a empty migrarion
typeorm migration:create ./src/migrations/MIGRATION-NAME

Generate new migrations based on schemas updates
npx typeorm-ts-node-esm migration:generate ./src/migrations/MIGRATION-NAME -d ./src/data-source.ts

Run Migrations
npx typeorm-ts-node-esm migration:run -d ./src/data-source.ts

Revert Migrations
npx typeorm-ts-node-esm migration:revert -d ./src/data-source.ts
