# imgb Globo

## Notas do Autor

O documento a seguir detalha o processo técnico utilizado para a construção de um projeto full stack, incluindo banco de dados, API e frontend. O documento está dividido em duas principais seções:

1. **Detalhamento Técnico:**

   -  Esta seção aborda os aspectos técnicos do projeto, fornecendo insights sobre a arquitetura, tecnologias utilizadas e outros detalhes relevantes.

2. **Possíveis Melhorias:**

   -  Nesta seção, são listadas as áreas que podem ser aprimoradas no projeto. Devido ao tempo limitado disponível, algumas melhorias podem não ter sido implementadas. Esta lista serve como um guia para futuras atualizações e otimizações.

3. **Como executar o projeto:**
   -  Nesta seção, serão listados a sequencia de procedimentos para a correta execução do projeto

# Detalhamento Técnico

## Tecnologias Escolhidas

-  **Banco de Dados:** MySQL (utilizando Docker Compose)
-  **Backend:** Node.js
-  **Frontend:** React.js
-  **Infraestrutura:** AWS
-  **Linguagem:** TypeScript

## Banco de Dados

A escolha do MySQL foi motivada pela familiaridade do autor com a tecnologia. No entanto, reconhece-se que, em um projeto real, o PostgreSQL poderia ser uma opção melhor devido ao seu desempenho superior em operações de gravações de alta frequência. Para a criação do banco de dados, foram utilizadas migrations que geram todas as tabelas e inserem os dados. Para a implantação, Docker Compose foi utilizado, contendo o PhpMyAdmin e o próprio banco MySQL.

## Tabelas Utilizadas

O projeto inicialmente poderia ter sido desenvolvido utilizando apenas duas tabelas, "usuarios" e "filmes". No entanto, visando uma melhor escalabilidade e organização, foram implementadas 11 tabelas, mantendo as relações entre elas de acordo com as regras estabelecidas.

1. **actors:** Tabela para armazenar informações sobre atores.
2. **directors:** Tabela para armazenar informações sobre diretores.
3. **genders:** Tabela para armazenar informações sobre gêneros.
4. **genres:** Tabela para armazenar informações sobre genêros (possivelmente um erro de digitação na nomenclatura).
5. **migrations:** Tabela utilizada para rastrear as migrações do banco de dados.
6. **movies:** Tabela principal para armazenar informações sobre filmes.
7. **movies_actors:** Tabela de relação entre filmes e atores.
8. **movies_directors:** Tabela de relação entre filmes e diretores.
9. **roles:** Tabela para armazenar informações sobre funções.
10.   **users:** Tabela para armazenar informações sobre usuários.
11.   **votes:** Tabela para armazenar informações sobre votos.

Cada uma dessas tabelas foi projetada para manter a consistência e integridade dos dados, permitindo uma estrutura robusta para o banco de dados do projeto.

## Autenticação

A autenticação no projeto foi implementada usando cookies com a flag HTTPOnly para reforçar a segurança. Essa escolha proporciona uma camada extra de proteção contra ataques XSS, já que os cookies HTTPOnly não são acessíveis via JavaScript, reduzindo o risco de roubo de tokens de autenticação por scripts maliciosos. Em comparação com a transmissão de tokens via header, a abordagem com cookies HTTPOnly é mais segura, minimizando a exposição a vulnerabilidades XSS e fortalecendo a integridade do sistema de autenticação. porém, para aplicações móveis nativas seria necessário a implementação de outro métodod e autenticação visto a falta de suporte a cookies das aplicações móveis nativas

## TypeScript

Atualmente, é sempre preferível iniciar projetos com TypeScript devido à sua capacidade de proporcionar maior escalabilidade, prevenindo erros em tempo de execução. Isso resulta em uma melhor manutenção e facilidade na testagem devido à tipagem estática.

## AWS

O serviço de computação em nuvem da Amazon (AWS) foi utilizado para armazenamento e computação. O S3 foi empregado para armazenamento de arquivos estáticos no momento de upload de imagens dos filmes, visando economizar recursos de armazenamento do servidor. A instância EC2 da AWS foi utilizada para a publicação do projeto na nuvem.

## Migrations

As migrations foram empregadas para a criação e manutenção da estrutura do banco de dados, facilitando a evolução e versionamento do esquema de banco de dados ao longo do tempo.

## S3

O Amazon S3 foi escolhido para armazenar arquivos estáticos, proporcionando escalabilidade e eficiência no gerenciamento de recursos durante o upload de imagens dos filmes.

## Docker Compose

O Docker Compose foi utilizado para simplificar o processo de implantação, proporcionando uma maneira consistente de empacotar e implantar o aplicativo, incluindo o PhpMyAdmin e o MySQL.

## Backend com TypeORM

O TypeORM foi escolhido devido à sua integração nativa com TypeScript. O padrão de projeto adotado para o backend segue as práticas do TypeORM, utilizando classes para o gerenciamento de entidades e controllers.

## Debounce

No front end, foi implementada a técnica de debounce na barra de pesquisa, assegurando uma economia de recursos computacionais ao reduzir a quantidade de chamadas de pesquisa, ao mesmo tempo em que aprimora a experiência do usuário final, tornando-a mais agradável.

## OpenAPI 3.0

A documentação da API segue o padrão OpenAPI 3.0, utilizando o Swagger para a interface e pode ser acessada através da URL /api-docs (não está ativa no ambiente hospedado)

## Rotas

O serviço conta com as seguintes rotas

1. GET /api/genders - Obtém todas as categorias de gênero
2. POST /api/genders - Cria uma nova categoria de gênero
3. GET /api/genders/{id} - Obtém uma categoria de gênero específica pelo ID
4. PATCH /api/genders/{id} - Atualiza uma categoria de gênero específica pelo ID
5. DELETE /api/genders/{id} - Exclui uma categoria de gênero específica pelo ID

6. GET /api/actors - Obtém todos os atores
7. POST /api/actors - Cria um novo ator
8. GET /api/actors/{id} - Obtém um ator específico pelo ID
9. PATCH /api/actors/{id} - Atualiza um ator específico pelo ID
10.   DELETE /api/actors/{id} - Exclui um ator específico pelo ID

11.   GET /api/directors - Obtém todos os diretores
12.   POST /api/directors - Cria um novo diretor
13.   GET /api/directors/{id} - Obtém um diretor específico pelo ID
14.   PATCH /api/directors/{id} - Atualiza um diretor específico pelo ID
15.   DELETE /api/directors/{id} - Exclui um diretor específico pelo ID

16.   GET /api/genres - Obtém todos os gêneros
17.   POST /api/genres - Cria um novo gênero
18.   GET /api/genres/{id} - Obtém um gênero específico pelo ID
19.   PATCH /api/genres/{id} - Atualiza um gênero específico pelo ID
20.   DELETE /api/genres/{id} - Exclui um gênero específico pelo ID

21.   GET /api/movies - Obtém todos os filmes
22.   POST /api/movies - Cria um novo filme
23.   GET /api/movies/{id} - Obtém um filme específico pelo ID
24.   PATCH /api/movies/{id} - Atualiza um filme específico pelo ID
25.   DELETE /api/movies/{id} - Exclui um filme específico pelo ID

26.   GET /api/roles - Obtém todas as funções
27.   POST /api/roles - Cria uma nova função
28.   GET /api/roles/{id} - Obtém uma função específica pelo ID
29.   PATCH /api/roles/{id} - Atualiza uma função específica pelo ID
30.   DELETE /api/roles/{id} - Exclui uma função específica pelo ID

31.   GET /api/users - Obtém todos os usuários
32.   POST /api/users - Cria um novo usuário
33.   GET /api/users/{id} - Obtém um usuário específico pelo ID
34.   PATCH /api/users/{id} - Atualiza um usuário específico pelo ID
35.   DELETE /api/users/{id} - Exclui lógicamente um usuário específico pelo ID

36.   GET /api/users/authenticate - Autentica um usuário
37.   GET /api/users/deauthenticate - Desautentica um usuário
38.   POST /api/users/activate/{id} - Ativa um usuário pelo ID

39.   POST /api/votes - Cria um novo voto

## Testes automatizados

No front end foram criados testes e2e utilizando a biblioteca cypress, em quanto no back end foram criados testes de integração utilizando a biblioteca Jest

## Segurança

1 - Foi utilizado a biblioteca helmet em conjunto com express para garantir uma camada de segurança contra possíveis vulnerabilidades conhecidas

2 - No front end não são armazenados nenhuma informação sensível do usuário

3 - Todas as rotas possuem uma validação de nível de acesso por usuário

# Possíveis melhorias

## Desempenho

1 - Optar pelo postgress seria uma melhor opção devido ao seu desempenho superior em operações de gravações de alta frequência

2 - Para a operação de contagem de votos poderia ter sido adotada uma estratégia de cacheamento para a tabela de votos, também poderia ter sido armazenado de maneira independente a média de votação de cada filme utilizando cacheamento desta tabela, e somente atualizando a média no banco de dados uma vez ao dia

3 - Toda a listagem de filmes poderia ter sido facilmente "cacheada" utilizando alguma tecnologia como redis

## Usuabilidade

1 - Melhorias no layout como responsividade seriam bem vindas visto que 85% dos acessos da internet brasileira são de dispositivos móveis

## Funcionamento

1 - Poderia ter sido incluída uma verificação para a não permitir mais de um voto por usuário por filme, evitando o acumulo de pontos em cada filme

2 - Apesar de ter sido criado 39 rotas, a maioria delas não foi utilizada, poderiam ser criadas muitas outras funcionalidades no painel de controle como: gerenciamento de atores, de diretores de gêneros e etc.

## Escala

1 - No front end poderia ter sido implementado alguma biblioteca de gerenciamento de estado tanto de cliente quanto de servidor como a React Query, porém devido ao tamanho do projeto não se fez necessário no momento atual

2 - Foram criados poucos testes devido ao prazo curto, porém seria necessário aumentar a cobertura de testes. Para o backend foram criados testes de integração em quanto para o front testes end-to-end, também poderia ter sido abordado uma estrégia de TDD para o futuro em caso de ganho de escala de equipe

# Como executar o projeto

1 - Clone este repositório

2 - Instale o docker e o docker compose

3 - entre na pasta server e crie o arquivo .env de variáveis de ambiente com as seguintes variaveis:

`S3_BUCKET` - bucket do s3 para armazenar imagens

`S3_SECRET_KEY` - o segredo de sua chave de segurança aws

`S3_ACCESS_KEY` - sua chave de segurança da aws

`MYSQL_ROOT_PASSWORD` - senha root do banco de dados

`MYSQL_DATABASE` - nome de sua base de dados

`MYSQL_USER` - usuário de sua base de dados

`MYSQL_PASSWORD` - senha padrão da sua base de dados

`MYSQL_PORT` - porta onde será executada sua base de dados

`LOG_FILEPATH` - caminho ara a pasta de logs

`SECRET_PASS` - segredo para criptografia dos cookies

`JWT_SECRET` - segredo para critografia do token jwt

`JWT_REFRESH_SECRET` - segredo ara a criptografia do token de auto revalidação

`COOKIE_SECURE` - se vai utilizar segurança nos cookies caso trabalhe com https digite true, caso contrário digite false

`BASE_PATH` - prefix da api, exemplo: /api

`PORT` - porta onde será executada sua aplicação

4 - execute os seguintes comandos:

-  `docker-compose up -d` para inicializar o banco de dados
-  `yarn install` para instalar os pacotes do servidor
-  `npx typeorm-ts-node-esm migration:run -d ./src/data-source.ts` para executar as migrations e inserir dados iniciais no banco de dados como os usuários de testes
-  `yarn dev` para executar o projeto

5 - entre na pasta client e crie o arquivo de variaveis de ambiente contendo os seguintes dados:

`REACT_APP_API` - endereço onde sua aplicação server esta rodando

`REACT_APP_SER_DATA_KEY` - chave onde sera armazenado um boleano para saber se o usuário esta logado ou não exemplo: "@isLoged", este campo não irá armazenar dados do usuário

6 - execute o comando `yarn start`

# Prévia

o projeto se encontra em execução no endereço http://54.211.156.82:3000/ por ser um servidor de teste pode se encontrar um pouco instável
